package pl.dziewulskij.passwordswallet.domain.user.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.core.common.LoginStatus;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "user_login_events")
public class UserLoginEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_login_events_generator_store")
	@GenericGenerator(
			name = "user_login_events_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "user_login_events_generator_seq"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 15)
	private LoginStatus loginStatus;

	@Column(nullable = false)
	private Long userId;

	@Column(nullable = false, length = 60)
	private String ipAddress;

	@Column(length = 100)
	private String operationSystem;

	@Column(length = 100)
	private String browserVersion;

	@Column(nullable = false)
	private LocalDateTime loginTime;

}
