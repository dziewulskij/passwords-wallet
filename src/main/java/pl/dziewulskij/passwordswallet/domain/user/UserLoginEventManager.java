package pl.dziewulskij.passwordswallet.domain.user;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.API.run;

import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.LoginStatus;
import pl.dziewulskij.passwordswallet.core.exception.security.AccountLockedException;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserLoginEventDaoService;

@Slf4j
@Component
public class UserLoginEventManager {

	private static final String USER_AGENT_HEADER_NAME = "User-Agent";

	private final UserLoginEventDaoService userLoginEventDaoService;
	private final UserDaoService userDaoService;

	public UserLoginEventManager(final UserLoginEventDaoService userLoginEventDaoService,
								 final UserDaoService userDaoService) {
		this.userLoginEventDaoService = userLoginEventDaoService;
		this.userDaoService = userDaoService;
	}

	public void checkIfAccountIsNonLocked(User user) {
		log.info("checkIfAccountIsNonLocked for user id: {}", user.getId());
		Optional.ofNullable(user.getUnlockedAt())
				.filter(unlockedAt -> LocalDateTime.now().isBefore(unlockedAt))
				.ifPresent(unlockedAt -> AccountLockedException.thrown("Account is locked to: " + user.getUnlockedAt()));
	}

	public void onFinallyAuthenticate(User user) {
		log.info("onFinallyAuthenticate for user id: {}", user.getId());
		var count = userLoginEventDaoService.find4LastLoginStatuses(user.getId()).stream()
				.takeWhile(loginStatus -> loginStatus != LoginStatus.SUCCESS)
				.count();

		Match(count).option(
				Case($(2L), o -> run(() -> setUnlockAt(user, 5))),
				Case($(3L), o -> run(() -> setUnlockAt(user, 10))),
				Case($(4L), o -> run(() -> setUnlockAt(user, 120)))
		);
	}

	public void loginEvent(HttpServletRequest httpServletRequest, User user, LoginStatus loginStatus) {
		log.info("loginEvent for user id: {} with status: {}", user.getId(), loginStatus);
		var userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader(USER_AGENT_HEADER_NAME));
		var userLoginEvent = UserLoginEvent.builder()
				.userId(user.getId())
				.loginStatus(loginStatus)
				.ipAddress(httpServletRequest.getRemoteAddr())
				.operationSystem(userAgent.getOperatingSystem().getName())
				.browserVersion(userAgent.getBrowser() + " " + userAgent.getBrowserVersion())
				.loginTime(LocalDateTime.now())
				.build();
		userLoginEventDaoService.save(userLoginEvent);
	}

	private void setUnlockAt(User user, int seconds) {
		user.setUnlockedAt(LocalDateTime.now().plusSeconds(seconds));
		userDaoService.save(user);
		AccountLockedException.thrown("Account is locked to: " + user.getUnlockedAt());
	}
}
