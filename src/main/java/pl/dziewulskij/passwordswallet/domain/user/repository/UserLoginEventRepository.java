package pl.dziewulskij.passwordswallet.domain.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent;

public interface UserLoginEventRepository extends JpaRepository<UserLoginEvent, Long> {

	List<UserLoginEvent> findTop4ByUserIdOrderByLoginTimeDesc(Long userId);

	List<UserLoginEvent> findAllByUserId(Long userId);

}
