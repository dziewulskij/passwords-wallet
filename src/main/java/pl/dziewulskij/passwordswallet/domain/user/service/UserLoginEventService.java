package pl.dziewulskij.passwordswallet.domain.user.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.mapper.user.UserLoginEventMapper;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserLoginEventDaoService;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserLoginEventDto;

@Slf4j
@Service
public class UserLoginEventService {

	private final UserLoginEventDaoService userLoginEventDaoService;
	private final UserLoginEventMapper userLoginEventMapper;

	public UserLoginEventService(final UserLoginEventDaoService userLoginEventDaoService,
								 final UserLoginEventMapper userLoginEventMapper) {
		this.userLoginEventDaoService = userLoginEventDaoService;
		this.userLoginEventMapper = userLoginEventMapper;
	}

	public List<UserLoginEventDto> getUserLoginEvents() {
		log.info("getUserLoginEvents");
		var userId = JwtUtil.getJwtUser().getId();
		return userLoginEventDaoService.findAllUserLoginEventByUserId(userId).stream()
				.map(userLoginEventMapper::mapToUserLoginEventDto)
				.collect(Collectors.toList());
	}
}
