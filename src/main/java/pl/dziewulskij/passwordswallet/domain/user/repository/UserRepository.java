package pl.dziewulskij.passwordswallet.domain.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String username);

	Optional<User> findByEmail(String email);

	boolean existsByUsername(String username);

	boolean existsByEmail(String email);

	@Modifying(clearAutomatically = true)
	@Query("update User u set u.walletAccessMode = ?2 where u.id = ?1")
	void updateWalletAccessMode(Long userId, WalletAccessMode walletAccessMode);
}
