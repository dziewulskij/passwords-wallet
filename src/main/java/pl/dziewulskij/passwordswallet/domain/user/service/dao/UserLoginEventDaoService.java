package pl.dziewulskij.passwordswallet.domain.user.service.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.LoginStatus;
import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent;
import pl.dziewulskij.passwordswallet.domain.user.repository.UserLoginEventRepository;

@Slf4j
@Service
public class UserLoginEventDaoService {

	private final UserLoginEventRepository userLoginEventRepository;

	public UserLoginEventDaoService(final UserLoginEventRepository userLoginEventRepository) {
		this.userLoginEventRepository = userLoginEventRepository;
	}

	public void save(UserLoginEvent userLoginEvent) {
		log.info("save: {}", userLoginEvent);
		userLoginEventRepository.save(userLoginEvent);
	}

	public List<LoginStatus> find4LastLoginStatuses(Long userId) {
		log.info("find4LastLoginStatuses for userId: {}", userId);
		return userLoginEventRepository.findTop4ByUserIdOrderByLoginTimeDesc(userId).stream()
				.map(UserLoginEvent::getLoginStatus)
				.collect(Collectors.toList());
	}

	public List<UserLoginEvent> findAllUserLoginEventByUserId(Long userId) {
		log.info("findAllUserLoginEventByUserId: {}", userId);
		return userLoginEventRepository.findAllByUserId(userId);
	}
}
