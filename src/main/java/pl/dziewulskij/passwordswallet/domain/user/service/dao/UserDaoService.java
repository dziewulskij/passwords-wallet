package pl.dziewulskij.passwordswallet.domain.user.service.dao;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.AuthorityName;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException;
import pl.dziewulskij.passwordswallet.core.secure.jwt.UserPrincipal;
import pl.dziewulskij.passwordswallet.domain.authority.AuthorityRepository;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.repository.UserRepository;

@Slf4j
@Service
public class UserDaoService {

	private final UserRepository userRepository;
	private final AuthorityRepository authorityRepository;

	public UserDaoService(final UserRepository userRepository,
						  final AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.authorityRepository = authorityRepository;
	}

	public Optional<UserPrincipal> findUserPrincipalByUsername(String username) {
		log.info("findUserPrincipalByUsername: {}", username);
		return userRepository.findByUsername(username)
				.map(UserPrincipal::build);
	}

	public Optional<User> findUserById(Long userId) {
		log.info("findUserById: {}", userId);
		return userRepository.findById(userId);
	}

	public Optional<User> findUserByUsername(String username) {
		log.info("findUserByUsername: {}", username);
		return userRepository.findByUsername(username);
	}

	public User createUser(User user) {
		log.info("createUser: {}", user);
		authorityRepository.findByAuthorityName(AuthorityName.ROLE_USER)
				.ifPresentOrElse(
						authority -> user.getAuthorities().add(authority),
						ObjectNotFoundException::thrown);
		return userRepository.save(user);
	}

	public User save(User user) {
		log.info("save: {}", user);
		return userRepository.save(user);
	}

	@Transactional
	public void updateWalletAccessMode(Long userId, WalletAccessMode walletAccessMode) {
		log.info("updateWalletAccessMode for: {}, mode: {}", userId, walletAccessMode);
		userRepository.updateWalletAccessMode(userId, walletAccessMode);
	}

}
