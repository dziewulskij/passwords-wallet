package pl.dziewulskij.passwordswallet.domain.user.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.domain.audit.AbstractDateAudit;
import pl.dziewulskij.passwordswallet.domain.authority.model.Authority;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "users")
public class User extends AbstractDateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_generator_store")
	@GenericGenerator(
			name = "users_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "users_generator_seq"),
					@Parameter(name = "initial_value", value = "10"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column(nullable = false, length = 50)
	private String name;

	@Column(updatable = false, nullable = false, length = 50)
	private String surname;

	@Column(nullable = false, unique = true, length = 30)
	private String username;

	@Column(updatable = false, nullable = false, unique = true, length = 100)
	private String email;

	@Column(name = "password_hash", nullable = false)
	private String password;

	@Column(nullable = false)
	private boolean enable;

	@Column(nullable = false)
	private boolean locked;

	@Column(nullable = false, length = 20)
	private String salt;

	@Column(nullable = false, length = 25)
	@Enumerated(EnumType.STRING)
	private Algorithm algorithm;

	private LocalDateTime unlockedAt;

	@Column(nullable = false, length = 25)
	@Enumerated(EnumType.STRING)
	private WalletAccessMode walletAccessMode;

	@Builder.Default
	@ManyToMany
	@Fetch(FetchMode.JOIN)
	@JoinTable(name = "users_authorities",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "authority_id"))
	private Set<Authority> authorities = new HashSet<>();

	@Builder.Default
	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private Set<Wallet> wallets = new HashSet<>();

	@Builder.Default
	@ManyToMany(mappedBy = "sharedForUsers")
	private Set<Wallet> sharedWallets = new HashSet<>();

	public void addWallet(Wallet wallet) {
		wallets.add(wallet);
		wallet.setUser(this);
	}

}
