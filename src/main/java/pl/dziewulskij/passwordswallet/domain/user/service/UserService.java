package pl.dziewulskij.passwordswallet.domain.user.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.core.mapper.user.UserMapper;
import pl.dziewulskij.passwordswallet.core.secure.SaltGenerator;
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserDto;

@Slf4j
@Service
public class UserService {

	private final UserMapper userMapper;
	private final UserDaoService userDaoService;
	private final HashStrategy hashStrategy;

	public UserService(final UserMapper userMapper,
					   final UserDaoService userDaoService,
					   final HashStrategy hashStrategy) {
		this.userMapper = userMapper;
		this.userDaoService = userDaoService;
		this.hashStrategy = hashStrategy;
	}

	public UserDto registerUser(RegisterUserDto registerUserDto) {
		log.info("registerUser: {}", registerUserDto);
		return Optional.of(registerUserDto)
				.map(userMapper::mapToUser)
				.map(this::prepareUser)
				.map(userDaoService::createUser)
				.map(userMapper::mapToUserDto)
				.orElseThrow();
	}

	public void changeWalletAccessMode(WalletAccessMode walletAccessMode) {
		log.info("changeWalletAccessMode: {}", walletAccessMode);
		userDaoService.updateWalletAccessMode(JwtUtil.getJwtUser().getId(), walletAccessMode);
	}

	private User prepareUser(User user) {
		var salt = SaltGenerator.generate();
		var hashedPassword = hashStrategy.compute(user.getAlgorithm(), user.getPassword(), salt);
		return user.toBuilder()
				.salt(salt)
				.password(hashedPassword)
				.build();
	}

}
