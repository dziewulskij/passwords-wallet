package pl.dziewulskij.passwordswallet.domain.authority;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.dziewulskij.passwordswallet.core.common.AuthorityName;
import pl.dziewulskij.passwordswallet.domain.authority.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

	Optional<Authority> findByAuthorityName(AuthorityName authorityName);

}
