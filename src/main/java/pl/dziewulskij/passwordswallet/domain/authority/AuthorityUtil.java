package pl.dziewulskij.passwordswallet.domain.authority;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.dziewulskij.passwordswallet.core.secure.jwt.UserPrincipal;
import pl.dziewulskij.passwordswallet.domain.authority.model.Authority;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class AuthorityUtil {

	public static Set<SimpleGrantedAuthority> getAuthorityNames(User user) {
		return user.getAuthorities().stream()
				.map(Authority::getAuthorityName)
				.map(Enum::name)
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toSet());
	}

	public static Set<String> getAuthorityNames(UserPrincipal userPrincipal) {
		return userPrincipal.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());
	}

}
