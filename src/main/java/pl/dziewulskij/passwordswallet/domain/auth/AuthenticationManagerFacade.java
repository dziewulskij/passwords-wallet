package pl.dziewulskij.passwordswallet.domain.auth;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.LoginStatus;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.domain.ipaddress.IpAddressEventManager;
import pl.dziewulskij.passwordswallet.domain.user.UserLoginEventManager;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService;

@Slf4j
@Component
class AuthenticationManagerFacade {

	private final UserLoginEventManager userLoginEventManager;
	private final IpAddressEventManager ipAddressEventManager;
	private final UserDaoService userDaoService;

	public AuthenticationManagerFacade(final UserLoginEventManager userLoginEventManager,
									   final IpAddressEventManager ipAddressEventManager,
									   final UserDaoService userDaoService) {
		this.userLoginEventManager = userLoginEventManager;
		this.ipAddressEventManager = ipAddressEventManager;
		this.userDaoService = userDaoService;
	}

	void checkIfIpAddressOrAccountIsNonLocked(HttpServletRequest httpServletRequest, User user) {
		log.info("checkIfIpAddressOrAccountIsNonLocked: {}, {}", httpServletRequest.getRemoteAddr(), user.getUsername());
		ipAddressEventManager.checkIfIpAddressIsNonLocked(httpServletRequest.getRemoteAddr());
		userLoginEventManager.checkIfAccountIsNonLocked(user);
	}

	void onSuccessAuthenticate(HttpServletRequest httpServletRequest, User user) {
		log.info("onSuccessAuthenticate: {}, {}", httpServletRequest.getRemoteAddr(), user.getUsername());
		Try.run(() -> userLoginEventManager.loginEvent(httpServletRequest, user, LoginStatus.SUCCESS))
				.andFinallyTry(() -> ipAddressEventManager.onSuccessAuthenticate(httpServletRequest))
				.onSuccess(val -> userDaoService.updateWalletAccessMode(user.getId(), WalletAccessMode.VIEW))
				.get();
	}

	void onFailureAuthenticate(HttpServletRequest httpServletRequest, User user) {
		log.info("onFailureAuthenticate: {}, {}", httpServletRequest.getRemoteAddr(), user.getUsername());
		Try.run(() -> userLoginEventManager.loginEvent(httpServletRequest, user, LoginStatus.ERROR))
				.andFinallyTry(() -> ipAddressEventManager.onFailureAuthenticate(httpServletRequest))
				.get();
	}

	void onFinallyAuthenticate(HttpServletRequest httpServletRequest, User user) {
		log.info("onFinallyAuthenticate: {}, {}", httpServletRequest.getRemoteAddr(), user.getUsername());
		Try.run(() -> userLoginEventManager.onFinallyAuthenticate(user))
				.andFinallyTry(() -> ipAddressEventManager.onFinallyAuthenticate(httpServletRequest))
				.get();
	}

}
