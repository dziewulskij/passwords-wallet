package pl.dziewulskij.passwordswallet.domain.auth;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService;
import pl.dziewulskij.passwordswallet.webui.rest.auth.dto.UserLoginDto;

@Slf4j
@Service
public class AuthenticationService {

	private final AuthenticationFacade authenticateFacade;
	private final UserDaoService userDaoService;
	private final AuthenticationManagerFacade authenticationManagerFacade;

	public AuthenticationService(final AuthenticationFacade authenticateFacade,
								 final UserDaoService userDaoService,
								 final AuthenticationManagerFacade authenticationManagerFacade) {
		this.authenticateFacade = authenticateFacade;
		this.userDaoService = userDaoService;
		this.authenticationManagerFacade = authenticationManagerFacade;
	}

	public String login(UserLoginDto userLoginDto, HttpServletRequest httpServletRequest) {
		log.info("login: {}", userLoginDto);
		return userDaoService.findUserByUsername(userLoginDto.getUsername())
				.map(user -> tryAuthenticateUser(userLoginDto, user, httpServletRequest))
				.orElseThrow(() -> new BadCredentialsException("Bad username"));
	}

	private String tryAuthenticateUser(UserLoginDto userLoginDto, User user, HttpServletRequest httpServletRequest) {
		authenticationManagerFacade.checkIfIpAddressOrAccountIsNonLocked(httpServletRequest, user);
		return Try.of(() -> tryAuthenticateUser(user, userLoginDto))
				.onSuccess(val -> authenticationManagerFacade.onSuccessAuthenticate(httpServletRequest, user))
				.onFailure(thr -> authenticationManagerFacade.onFailureAuthenticate(httpServletRequest, user))
				.andFinally(() -> authenticationManagerFacade.onFinallyAuthenticate(httpServletRequest, user))
				.get();
	}

	private String tryAuthenticateUser(User user, UserLoginDto userLoginDto) {
		return Optional.of(user)
				.map(usr -> authenticateFacade.compute(user.getAlgorithm(), userLoginDto.getPassword(), usr.getSalt()))
				.map(password -> buildAuthenticate(userLoginDto.getUsername(), password))
				.map(this::setSecurityContextAndGenerateToken)
				.orElseThrow(() -> new BadCredentialsException("Bad credentials"));
	}

	private Authentication buildAuthenticate(String username, String password) {
		var usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password);
		return authenticateFacade.authenticate(usernamePasswordAuthenticationToken);
	}

	private String setSecurityContextAndGenerateToken(Authentication authentication) {
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return authenticateFacade.generateToken(authentication);
	}

}
