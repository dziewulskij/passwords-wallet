package pl.dziewulskij.passwordswallet.domain.auth;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtTokenProvider;

@Slf4j
@Component
public class AuthenticationFacade {

	private final AuthenticationManager authenticationManager;
	private final JwtTokenProvider jwtTokenProvider;
	private final HashStrategy hashStrategy;

	public AuthenticationFacade(final AuthenticationManager authenticationManager,
								final JwtTokenProvider jwtTokenProvider,
								final HashStrategy hashStrategy) {
		this.authenticationManager = authenticationManager;
		this.jwtTokenProvider = jwtTokenProvider;
		this.hashStrategy = hashStrategy;
	}

	String compute(Algorithm algorithm, String password, String salt) {
		log.info("compute with algorithm: {}", algorithm);
		return hashStrategy.compute(algorithm, password, salt);
	}

	Authentication authenticate(UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) {
		log.info("authenticate");
		return authenticationManager.authenticate(usernamePasswordAuthenticationToken);
	}

	String generateToken(Authentication authentication) {
		log.info("generateToken");
		return jwtTokenProvider.generateToken(authentication);
	}

}
