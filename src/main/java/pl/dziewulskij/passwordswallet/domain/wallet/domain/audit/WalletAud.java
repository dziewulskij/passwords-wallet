package pl.dziewulskij.passwordswallet.domain.wallet.domain.audit;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.core.common.ActionType;
import pl.dziewulskij.passwordswallet.domain.audit.AbstractDateAudit;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "wallets_aud")
public class WalletAud extends AbstractDateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallets_aud_generator_store")
	@GenericGenerator(
			name = "wallets_aud_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "wallets_aud_generator_seq"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column(nullable = false, length = 30)
	private String login;

	@Column(name = "password_encrypted", nullable = false, length = 256)
	private String password;

	@Column(nullable = false, length = 256)
	private String webAddress;

	@Column(length = 256)
	private String description;

	@Column(nullable = false)
	private boolean removed;

	@Enumerated(EnumType.STRING)
	private ActionType actionType;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "wallet_id", nullable = false)
	private Wallet wallet;

	@Builder.Default
	@OneToMany(mappedBy = "walletAud", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Set<UserSharedWalletAud> userSharedWalletAudits = new HashSet<>();

	public void addUserSharedWalletAud(UserSharedWalletAud userSharedWalletAud) {
		userSharedWalletAudits.add(userSharedWalletAud);
		userSharedWalletAud.setWalletAud(this);
	}
}
