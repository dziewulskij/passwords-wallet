package pl.dziewulskij.passwordswallet.domain.wallet.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud;

public interface WalletAudRepository extends JpaRepository<WalletAud, Long> {

	@Query("select w.id from WalletAud wa left join wa.wallet w where wa.id = ?1")
	Optional<Long> findWalletAudWalletId(Long walletAudId);

}
