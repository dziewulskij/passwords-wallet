package pl.dziewulskij.passwordswallet.domain.wallet.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.ActionType;
import pl.dziewulskij.passwordswallet.core.mapper.wallet.WalletMapper;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.UserSharedWalletAud;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud;
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletAudRepository;

@Slf4j
@Service
public class WalletAudService {

	private final WalletAudRepository walletAudRepository;
	private final WalletMapper walletMapper;

	public WalletAudService(final WalletAudRepository walletAudRepository,
							final WalletMapper walletMapper) {
		this.walletAudRepository = walletAudRepository;
		this.walletMapper = walletMapper;
	}

	@Transactional
	public void create(Wallet wallet, ActionType actionType) {
		log.info("create wallet audit for: {}, with actionType: {}", wallet, actionType);
		var walletAud = walletMapper.mapToWalletAud(wallet).toBuilder()
				.actionType(actionType)
				.build();

		wallet.getSharedForUsers().stream()
				.map(user -> buildSharedWalletAud(wallet, user))
				.forEach(walletAud::addUserSharedWalletAud);

		walletAudRepository.save(walletAud);
	}

	public Optional<WalletAud> findWalletAudById(Long id) {
		log.info("findWalletAudById: {}", id);
		return walletAudRepository.findById(id);
	}

	private UserSharedWalletAud buildSharedWalletAud(Wallet wallet, User user) {
		return UserSharedWalletAud.builder()
				.user(user)
				.wallet(wallet)
				.build();
	}

}
