package pl.dziewulskij.passwordswallet.domain.wallet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;

public interface WalletRepository extends JpaRepository<Wallet, Long> {

	@Query("select w from Wallet w where w.user.id = ?1")
	List<Wallet> findAllWalletsByUserId(Long userId);

	@Query("select w.user.id from Wallet w where w.id = ?1")
	Optional<Long> findOwnerIdByWalletId(Long walletId);

	@Query("select w from Wallet w left join fetch w.sharedForUsers sfu where sfu.id = ?1")
	List<Wallet> findAllSharedWalletsFor(Long userId);

}
