package pl.dziewulskij.passwordswallet.domain.wallet.service;

import java.security.Key;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import static pl.dziewulskij.passwordswallet.core.common.ActionType.UPDATE;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException;
import pl.dziewulskij.passwordswallet.core.mapper.wallet.WalletMapper;
import pl.dziewulskij.passwordswallet.core.secure.AESenc;
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.UserSharedWalletAud;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.SimpleWalletDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletContainerDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletDto;

@Slf4j
@Service
public class WalletService {

	private final WalletDaoService walletDaoService;
	private final WalletAudService walletAudService;
	private final WalletMapper walletMapper;
	private final HashStrategy hashStrategy;

	public WalletService(final WalletDaoService walletDaoService,
						 final WalletAudService walletAudService,
						 final WalletMapper walletMapper,
						 final HashStrategy hashStrategy) {
		this.walletDaoService = walletDaoService;
		this.walletMapper = walletMapper;
		this.hashStrategy = hashStrategy;
		this.walletAudService = walletAudService;
	}

	public WalletDto createWallet(SimpleWalletDto simpleWalletDto) {
		log.info("createWallet: {}", simpleWalletDto);
		return Optional.of(simpleWalletDto)
				.map(walletMapper::mapToWallet)
				.map(this::encryptWalletPassword)
				.map(walletDaoService::createWallet)
				.map(walletMapper::mapToWalletDto)
				.orElseThrow();
	}

	public WalletDto getWalletById(Long id) {
		log.info("getWalletById: {}", id);
		return walletDaoService.findWalletById(id)
				.map(this::decryptPasswordWallet)
				.map(walletMapper::mapToWalletDto)
				.orElseThrow(ObjectNotFoundException::new);
	}

	public WalletContainerDto getAllUserWallets() {
		log.info("getAllUserWallets");
		var userId = JwtUtil.getJwtUser().getId();
		var onwWallets = walletDaoService.findAllUserWallets(userId).stream()
				.map(walletMapper::mapToWalletDto)
				.collect(Collectors.toList());

		var sharedWallets = walletDaoService.findAllUserSharedWallets(userId).stream()
				.map(walletMapper::mapToWalletDto)
				.collect(Collectors.toList());

		return WalletContainerDto.builder()
				.ownWallets(onwWallets)
				.sharedWallets(sharedWallets)
				.build();
	}

	public WalletDto editWalletById(Long id, SimpleWalletDto simpleWalletDto) {
		log.info("editWalletById: {}, object: {}", id, simpleWalletDto);
		return walletDaoService.findWalletById(id)
				.map(wallet -> walletMapper.mapToWallet(simpleWalletDto, wallet))
				.map(this::encryptWalletPassword)
				.map(wallet -> walletDaoService.save(wallet, UPDATE))
				.map(walletMapper::mapToWalletDto)
				.orElseThrow(ObjectNotFoundException::new);
	}

	public void deleteWalletById(Long id) {
		log.info("deleteWalletById: {}", id);
		walletDaoService.findWalletById(id)
				.ifPresentOrElse(walletDaoService::delete, ObjectNotFoundException::thrown);
	}

	public void shareWalletByIdFor(Long walletId, String email) {
		log.info("shareWalletByIdFor: {}, for: {}", walletId, email);
		walletDaoService.findWalletById(walletId)
				.map(wallet -> walletDaoService.shareWalletFor(wallet, email))
				.orElseThrow(ObjectNotFoundException::new);
	}

	public void rejectSharedWalletByIdFor(Long walletId, Long userId) {
		log.info("rejectSharedWalletByIdFor: {}, for: {}", walletId, userId);
		walletDaoService.findWalletById(walletId)
				.map(wallet -> walletDaoService.rejectSharedWalletFor(wallet, userId))
				.orElseThrow(ObjectNotFoundException::new);
	}

	public WalletDto rollbackWalletByWalletAudId(Long walletAudId) {
		log.info("rollbackWalletByWalletAudId: {}", walletAudId);
		return walletAudService.findWalletAudById(walletAudId)
				.map(this::prepareWalletToRollback)
				.map(wallet -> walletDaoService.save(wallet, UPDATE))
				.map(walletMapper::mapToWalletDto)
				.orElseThrow(ObjectNotFoundException::new);
	}

	private Wallet encryptWalletPassword(Wallet wallet) {
		var password = JwtUtil.getJwtUser().getPassword();
		var encryptedPassword = AESenc.encrypt(wallet.getPassword(), generateKey(password));
		return wallet.toBuilder()
				.password(encryptedPassword)
				.build();
	}

	private Wallet decryptPasswordWallet(Wallet wallet) {
		var jwtUser = JwtUtil.getJwtUser();
		var password = Optional.of(jwtUser.getId())
				.filter(id -> wallet.getUser().getId().equals(id))
				.map(aLong -> jwtUser.getPassword())
				.orElse(wallet.getUser().getPassword());

		var decryptedPassword = AESenc.decrypt(wallet.getPassword(), generateKey(password));
		return wallet.toBuilder()
				.password(decryptedPassword)
				.build();
	}

	private Key generateKey(String password) {
		var md5HashPassword = hashStrategy.compute(Algorithm.MD5, password, "");
		return AESenc.generateKey(md5HashPassword);
	}

	private Wallet prepareWalletToRollback(pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud walletAud) {
		var wallet = walletMapper.mapToWallet(walletAud, walletAud.getWallet());
		wallet.getSharedForUsers().clear();

		walletAud.getUserSharedWalletAudits().stream()
				.map(UserSharedWalletAud::getUser)
				.forEach(wallet::addUserToSharedFor);

		return wallet;
	}

}
