package pl.dziewulskij.passwordswallet.domain.wallet.domain.audit;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;

@Getter
@Setter
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "users_shared_wallets_aud")
public class UserSharedWalletAud {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usw_aud_generator_store")
	@GenericGenerator(
			name = "usw_aud_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "usw_aud_generator_seq"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@ManyToOne
	private User user;

	@ManyToOne
	private Wallet wallet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false, name = "wallet_aud_id")
	private WalletAud walletAud;

}
