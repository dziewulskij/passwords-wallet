package pl.dziewulskij.passwordswallet.domain.wallet.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.domain.audit.AbstractAudit;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Getter
@Setter
@Entity
@ToString(of = "id")
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "wallets")
@Where(clause = "removed != true")
public class Wallet extends AbstractAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallets_generator_store")
	@GenericGenerator(
			name = "wallets_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "wallets_generator_seq"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column(nullable = false, length = 30)
	private String login;

	@Column(nullable = false, length = 256)
	private String password;

	@Column(nullable = false, length = 256)
	private String webAddress;

	@Column(length = 256)
	private String description;

	@Column(nullable = false)
	private boolean removed;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	@Builder.Default
	@ManyToMany
	@JoinTable(name = "shared_wallets_users",
			joinColumns = @JoinColumn(name = "wallet_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> sharedForUsers = new HashSet<>();

	public void addUserToSharedFor(User user) {
		sharedForUsers.add(user);
		user.getSharedWallets().add(this);
	}

	public void removeUserFromSharedFor(User user) {
		sharedForUsers.remove(user);
		user.getSharedWallets().remove(this);
	}
}
