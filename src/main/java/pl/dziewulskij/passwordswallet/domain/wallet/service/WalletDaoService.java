package pl.dziewulskij.passwordswallet.domain.wallet.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import static pl.dziewulskij.passwordswallet.core.common.ActionType.CREATE;
import static pl.dziewulskij.passwordswallet.core.common.ActionType.DELETE;
import static pl.dziewulskij.passwordswallet.core.common.ActionType.UPDATE;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.ActionType;
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.user.repository.UserRepository;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletRepository;

@Slf4j
@Service
public class WalletDaoService {

	private final UserRepository userRepository;
	private final WalletRepository walletRepository;
	private final WalletAudService walletAudService;

	public WalletDaoService(final UserRepository userRepository,
							final WalletRepository walletRepository,
							final WalletAudService walletAudService) {
		this.userRepository = userRepository;
		this.walletRepository = walletRepository;
		this.walletAudService = walletAudService;
	}

	@Transactional
	public Wallet createWallet(Wallet wallet) {
		log.info("createWallet: {}", wallet);
		var userId = JwtUtil.getJwtUser().getId();
		userRepository.findById(userId)
				.ifPresentOrElse(user -> user.addWallet(wallet), ObjectNotFoundException::thrown);
		var savedWallet = walletRepository.save(wallet);
		walletAudService.create(savedWallet, CREATE);
		return savedWallet;
	}

	public Optional<Wallet> findWalletById(Long id) {
		log.info("findWalletById: {}", id);
		return walletRepository.findById(id);
	}

	public List<Wallet> findAllUserWallets(Long userId) {
		log.info("findAllUserWallets: {}", userId);
		return walletRepository.findAllWalletsByUserId(userId);
	}

	public List<Wallet> findAllUserSharedWallets(Long userId) {
		log.info("findAllUserSharedWallets: {}", userId);
		return walletRepository.findAllSharedWalletsFor(userId);
	}

	@Transactional
	public Wallet save(Wallet wallet, ActionType actionType) {
		log.info("save: {}", wallet);
		var savedWallet = walletRepository.save(wallet);
		walletAudService.create(savedWallet, actionType);
		return savedWallet;
	}

	public void delete(Wallet wallet) {
		log.info("delete: {}", wallet);
		wallet.setRemoved(true);
		var removedWallet = walletRepository.save(wallet);
		walletAudService.create(removedWallet, DELETE);
	}

	@Transactional
	public Wallet shareWalletFor(Wallet wallet, String email) {
		log.info("sharePassword: {}, for: {}", wallet, email);
		userRepository.findByEmail(email)
				.ifPresentOrElse(
						wallet::addUserToSharedFor,
						ObjectNotFoundException::thrown);

		var savedWallet = walletRepository.save(wallet);
		walletAudService.create(savedWallet, UPDATE);
		return savedWallet;
	}

	public Wallet rejectSharedWalletFor(Wallet wallet, Long userId) {
		log.info("rejectSharedWalletByIdFor: {}, for: {}", wallet, userId);
		wallet.getSharedForUsers().stream()
				.filter(user -> user.getId().equals(userId))
				.findFirst()
				.ifPresent(wallet::removeUserFromSharedFor);

		var savedWallet = walletRepository.save(wallet);
		walletAudService.create(savedWallet, UPDATE);
		return savedWallet;
	}

}
