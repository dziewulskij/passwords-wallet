package pl.dziewulskij.passwordswallet.domain.ipaddress.service;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException;
import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress;

@Slf4j
@Service
public class IpAddressService {

	private final IpAddressDaoService ipAddressDaoService;

	public IpAddressService(final IpAddressDaoService ipAddressDaoService) {
		this.ipAddressDaoService = ipAddressDaoService;
	}

	public void unlockByIpAddress(String ipAddress) {
		log.info("unlockByIpAddress: {}", ipAddress);
		ipAddressDaoService.findByIpAddress(ipAddress)
				.map(this::prepareToUnlock)
				.map(ipAddressDaoService::save)
				.orElseThrow(ObjectNotFoundException::new);
	}

	private IpAddress prepareToUnlock(IpAddress ipAddress) {
		ipAddress.setPermanentlyLocked(false);
		ipAddress.setUnlockedAt(null);
		ipAddress.clearIncorrectAttempts();
		return ipAddress;
	}

}
