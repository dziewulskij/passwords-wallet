package pl.dziewulskij.passwordswallet.domain.ipaddress;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.function.Predicate;

import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress;

public class CheckLockPredicate implements Predicate<IpAddress> {

	@Override
	public boolean test(IpAddress ipAddress) {
		return Objects.nonNull(ipAddress.getUnlockedAt())
				&& (LocalDateTime.now().isBefore(ipAddress.getUnlockedAt()) || ipAddress.isPermanentlyLocked());
	}

}
