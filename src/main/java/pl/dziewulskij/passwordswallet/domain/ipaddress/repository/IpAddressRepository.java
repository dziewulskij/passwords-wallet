package pl.dziewulskij.passwordswallet.domain.ipaddress.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress;

public interface IpAddressRepository extends JpaRepository<IpAddress, Long> {

	Optional<IpAddress> findByIpAddress(String ipAddress);

}
