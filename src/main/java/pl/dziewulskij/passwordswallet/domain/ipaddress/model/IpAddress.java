package pl.dziewulskij.passwordswallet.domain.ipaddress.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.domain.audit.AbstractDateAudit;

@Getter
@Setter
@Entity
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Table(name = "ip_addresses")
public class IpAddress extends AbstractDateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ip_addresses_generator_store")
	@GenericGenerator(
			name = "ip_addresses_generator_store",
			strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
			parameters = {
					@Parameter(name = "sequence_name", value = "ip_addresses_generator_seq"),
					@Parameter(name = "initial_value", value = "1"),
					@Parameter(name = "increment_size", value = "1")
			}
	)
	private Long id;

	@Column(nullable = false, unique = true, length = 60)
	private String ipAddress;

	private LocalDateTime unlockedAt;

	@Column(nullable = false)
	private Integer incorrectAttempts;

	@Column(nullable = false)
	private boolean permanentlyLocked;

	public void increaseIncorrectAttempts() {
		incorrectAttempts++;
	}

	public void clearIncorrectAttempts() {
		incorrectAttempts = 0;
	}

}
