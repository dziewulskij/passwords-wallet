package pl.dziewulskij.passwordswallet.domain.ipaddress.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress;
import pl.dziewulskij.passwordswallet.domain.ipaddress.repository.IpAddressRepository;

@Slf4j
@Service
public class IpAddressDaoService {

	private final IpAddressRepository ipAddressRepository;

	public IpAddressDaoService(final IpAddressRepository ipAddressRepository) {
		this.ipAddressRepository = ipAddressRepository;
	}

	public Optional<IpAddress> findByIpAddress(String remoteAddr) {
		log.info("findByIpAddress: {}", remoteAddr);
		return ipAddressRepository.findByIpAddress(remoteAddr);
	}

	public IpAddress save(IpAddress ipAddress) {
		log.info("save: {}", ipAddress);
		return ipAddressRepository.save(ipAddress);
	}

}
