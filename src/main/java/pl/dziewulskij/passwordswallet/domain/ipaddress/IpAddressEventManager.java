package pl.dziewulskij.passwordswallet.domain.ipaddress;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.API.run;
import static java.time.LocalDateTime.now;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.exception.security.IpAddressLockedException;
import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress;
import pl.dziewulskij.passwordswallet.domain.ipaddress.service.IpAddressDaoService;

@Slf4j
@Component
public class IpAddressEventManager {

	private final IpAddressDaoService ipAddressDaoService;
	private final CheckLockPredicate CHECK_LOCK_PREDICATE;

	public IpAddressEventManager(IpAddressDaoService ipAddressDaoService) {
		this.ipAddressDaoService = ipAddressDaoService;
		CHECK_LOCK_PREDICATE = new CheckLockPredicate();
	}

	public void checkIfIpAddressIsNonLocked(String remoteAddr) {
		log.info("checkIfIpAddressIsNonLocked for ip address: {}", remoteAddr);
		ipAddressDaoService.findByIpAddress(remoteAddr)
				.filter(CHECK_LOCK_PREDICATE::test)
				.ifPresent(ipAddress -> IpAddressLockedException.thrown(remoteAddr, ipAddress.getUnlockedAt()));
	}

	public void onSuccessAuthenticate(HttpServletRequest httpServletRequest) {
		log.info("onSuccessAuthenticate for ip address: {}", httpServletRequest.getRemoteAddr());
		var remoteAddr = httpServletRequest.getRemoteAddr();
		ipAddressDaoService.findByIpAddress(remoteAddr)
				.ifPresent(ipAddr -> prepareAndSave(ipAddr, ipAddr::clearIncorrectAttempts));
	}

	public void onFailureAuthenticate(HttpServletRequest httpServletRequest) {
		log.info("onFailureAuthenticate for ip address: {}", httpServletRequest.getRemoteAddr());
		var remoteAddr = httpServletRequest.getRemoteAddr();
		ipAddressDaoService.findByIpAddress(remoteAddr)
				.ifPresentOrElse(
						ipAddr -> prepareAndSave(ipAddr, ipAddr::increaseIncorrectAttempts),
						() -> ipAddressDaoService.save(buildIpAddress(remoteAddr))
				);
	}

	public void onFinallyAuthenticate(HttpServletRequest httpServletRequest) {
		log.info("onFinallyAuthenticate for ip address: {}", httpServletRequest.getRemoteAddr());
		ipAddressDaoService.findByIpAddress(httpServletRequest.getRemoteAddr())
				.ifPresent(ipAddress ->
						Match(ipAddress.getIncorrectAttempts()).option(
								Case($(2), o -> run(() -> setUnlockAt(ipAddress, 5))),
								Case($(3), o -> run(() -> setUnlockAt(ipAddress, 10))),
								Case($(4), o -> run(() -> setPermanentlyBlocked(ipAddress)))
						)
				);
	}

	private void prepareAndSave(IpAddress ipAddr, Runnable runnable) {
		runnable.run();
		ipAddressDaoService.save(ipAddr);
	}

	private void setUnlockAt(IpAddress ipAddress, int seconds) {
		ipAddress.setUnlockedAt(now().plusSeconds(seconds));
		ipAddressDaoService.save(ipAddress);
		IpAddressLockedException.thrown("Ip address " + ipAddress.getIpAddress() + " is locked to: " + ipAddress.getUnlockedAt());
	}

	private void setPermanentlyBlocked(IpAddress ipAddress) {
		ipAddress.setPermanentlyLocked(true);
		ipAddressDaoService.save(ipAddress);
		IpAddressLockedException.thrown("Ip address " + ipAddress.getIpAddress() + " is locked permanently");
	}

	private IpAddress buildIpAddress(String remoteAddr) {
		return IpAddress.builder()
				.ipAddress(remoteAddr)
				.incorrectAttempts(1)
				.build();
	}

}
