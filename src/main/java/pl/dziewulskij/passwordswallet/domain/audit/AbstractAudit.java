package pl.dziewulskij.passwordswallet.domain.audit;

import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Setter
@Getter
@MappedSuperclass
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
		value = {"createdBy", "lastModifyBy"},
		allowGetters = true
)
public abstract class AbstractAudit extends AbstractDateAudit {

	@CreatedBy
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "created_by", nullable = false)
	private User createdBy;

	@LastModifiedBy
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "last_modified_by", nullable = false)
	private User lastModifiedBy;

}
