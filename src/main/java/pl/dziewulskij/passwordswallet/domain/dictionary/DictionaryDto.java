package pl.dziewulskij.passwordswallet.domain.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Builder(toBuilder = true)
public class DictionaryDto {

	private Long id;
	private String name;

}
