package pl.dziewulskij.passwordswallet.core.mapper.user;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserDto;

@Mapper
public interface UserMapper {

	UserDto mapToUserDto(User user);

	@Mapping(target = "enable", constant = "true")
	@Mapping(target = "locked", constant = "false")
	@Mapping(target = "walletAccessMode", constant = "VIEW")
	User mapToUser(RegisterUserDto registerUserDto);

}
