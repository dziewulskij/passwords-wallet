package pl.dziewulskij.passwordswallet.core.mapper.user;

import org.mapstruct.Mapper;

import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserLoginEventDto;

@Mapper
public interface UserLoginEventMapper {

	UserLoginEventDto mapToUserLoginEventDto(UserLoginEvent userLoginEvent);

}
