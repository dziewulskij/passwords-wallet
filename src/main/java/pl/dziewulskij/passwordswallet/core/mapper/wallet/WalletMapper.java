package pl.dziewulskij.passwordswallet.core.mapper.wallet;

import org.mapstruct.AfterMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import pl.dziewulskij.passwordswallet.core.mapper.dictionary.DictionaryMapper;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.SimpleWalletDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletDto;

@Mapper(uses = DictionaryMapper.class, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface WalletMapper {

	Wallet mapToWallet(SimpleWalletDto simpleWalletDto);

	Wallet mapToWallet(SimpleWalletDto simpleWalletDto, @MappingTarget Wallet wallet);

	@Mapping(source = "user", target = "owner")
	WalletDto mapToWalletDto(Wallet wallet);

	@Mapping(target = "id", ignore = true)
	WalletAud mapToWalletAud(Wallet wallet);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	Wallet mapToWallet(WalletAud walletAud, @MappingTarget Wallet wallet);

	@AfterMapping
	default void setWallet(Wallet wallet, @MappingTarget WalletAud walletAud) {
		walletAud.setWallet(wallet);
	}


}
