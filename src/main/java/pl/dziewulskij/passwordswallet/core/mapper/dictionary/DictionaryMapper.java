package pl.dziewulskij.passwordswallet.core.mapper.dictionary;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import pl.dziewulskij.passwordswallet.domain.dictionary.DictionaryDto;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Mapper
public interface DictionaryMapper {

	@Mapping(target = "name", expression = "java(user.getName() + \" \" + user.getSurname())")
	DictionaryDto mapToDictionaryDto(User user);

}
