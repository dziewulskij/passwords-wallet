package pl.dziewulskij.passwordswallet.core.exception.security;

public class AccountLockedException extends RuntimeException {

	public AccountLockedException() {
	}

	public AccountLockedException(String message) {
		super(message);
	}

	public static void thrown(String message) {
		throw new AccountLockedException(message);
	}

}
