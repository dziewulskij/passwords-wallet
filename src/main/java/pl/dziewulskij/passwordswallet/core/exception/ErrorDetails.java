package pl.dziewulskij.passwordswallet.core.exception;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorDetails {

	private final String field;
	private final String code;

}
