package pl.dziewulskij.passwordswallet.core.exception.security;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import pl.dziewulskij.passwordswallet.core.exception.ErrorResponse;

@ControllerAdvice
public class SecurityControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AccountLockedException.class)
	public ResponseEntity<Object> handleCityNotFoundException(AccountLockedException accountLockedException) {
		var errorResponse = buildErrorResponse(accountLockedException.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(IpAddressLockedException.class)
	public ResponseEntity<Object> handleIpAddressLockedException(IpAddressLockedException ipAddressLockedException) {
		var errorResponse = buildErrorResponse(ipAddressLockedException.getMessage());
		return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
	}

	private ErrorResponse buildErrorResponse(String message) {
		return ErrorResponse.builder()
				.timestamp(LocalDateTime.now())
				.message(message)
				.build();
	}

}
