package pl.dziewulskij.passwordswallet.core.exception.security;

import java.time.LocalDateTime;

public class IpAddressLockedException extends RuntimeException {

	public IpAddressLockedException() {
	}

	public IpAddressLockedException(String message) {
		super(message);
	}

	public static void thrown(String message) {
		throw new IpAddressLockedException(message);
	}

	public static void thrown(String ipAddress, LocalDateTime unlockedAt) {
		throw new IpAddressLockedException("Ip address " + ipAddress + " is locked to: " + unlockedAt + ", or permanently");
	}

}
