package pl.dziewulskij.passwordswallet.core.exception;

import java.time.LocalDateTime;
import java.util.Set;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ErrorResponse {

	private final LocalDateTime timestamp;
	private final String message;
	private final Set<ErrorDetails> errorDetails;

	public static ErrorResponse build(Set<ErrorDetails> errorDetails) {
		return ErrorResponse.builder()
				.timestamp(LocalDateTime.now())
				.errorDetails(errorDetails)
				.build();
	}
}
