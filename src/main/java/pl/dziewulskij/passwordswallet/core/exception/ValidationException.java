package pl.dziewulskij.passwordswallet.core.exception;

public class ValidationException extends RuntimeException {

	public ValidationException() {
	}

	public ValidationException(String message) {
		super(message);
	}

}
