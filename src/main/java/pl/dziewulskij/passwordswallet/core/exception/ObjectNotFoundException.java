package pl.dziewulskij.passwordswallet.core.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObjectNotFoundException extends RuntimeException {

	private static final String DEFAULT_MESSAGE = "Object not found";
	private static final String DEFAULT_MESSAGE_WITH_ID = "Object not found with id ";

	public ObjectNotFoundException() {
		super(DEFAULT_MESSAGE);
	}

	public ObjectNotFoundException(Long id) {
		super(DEFAULT_MESSAGE_WITH_ID + id);
	}


	public static void thrown() {
		log.error("throwObjectNotFoundException");
		throw new ObjectNotFoundException();
	}
}
