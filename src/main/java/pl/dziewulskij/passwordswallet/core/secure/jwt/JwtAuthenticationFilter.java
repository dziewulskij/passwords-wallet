package pl.dziewulskij.passwordswallet.core.secure.jwt;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService;

@Slf4j
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final UserDaoService userDaoService;
	private final JwtTokenProvider jwtTokenProvider;

	public JwtAuthenticationFilter(final UserDaoService userDaoService,
								   final JwtTokenProvider jwtTokenProvider) {
		this.userDaoService = userDaoService;
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest,
									HttpServletResponse httpServletResponse,
									FilterChain filterChain) throws ServletException, IOException {
		Try.run(() -> doFilter(httpServletRequest))
				.onFailure(this::logError);
		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	private void doFilter(HttpServletRequest httpServletRequest) {
		JwtUtil.resolveToken(httpServletRequest)
				.filter(token -> StringUtils.hasText(token) && jwtTokenProvider.validateToken(token))
				.flatMap(JwtUtil::getUserId)
				.flatMap(userDaoService::findUserById)
				.ifPresent(user -> assignAuthenticationSecurityContext(user, httpServletRequest));
	}

	private void assignAuthenticationSecurityContext(User user, HttpServletRequest httpServletRequest) {
		var jwtUser = JwtUser.build(user);
		var authentication = new UsernamePasswordAuthenticationToken(jwtUser, null, toSimpleGrantedAuthorities(user));
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	private Set<SimpleGrantedAuthority> toSimpleGrantedAuthorities(User user) {
		return user.getAuthorities().stream()
				.map(authority -> authority.getAuthorityName().name())
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toSet());
	}

	private void logError(Throwable thr) {
		logger.error("Could not set user authentication in security context", thr);
	}

}
