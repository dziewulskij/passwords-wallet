package pl.dziewulskij.passwordswallet.core.secure.jwt;

import lombok.Builder;
import lombok.Getter;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Getter
@Builder
public final class JwtUser {

	private final Long id;
	private final String email;
	private final String password;
	private final WalletAccessMode walletAccessMode;

	public static JwtUser build(User user) {
		return JwtUser.builder()
				.id(user.getId())
				.email(user.getEmail())
				.password(user.getPassword())
				.walletAccessMode(user.getWalletAccessMode())
				.build();
	}

}