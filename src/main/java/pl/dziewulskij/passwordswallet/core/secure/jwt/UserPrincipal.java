package pl.dziewulskij.passwordswallet.core.secure.jwt;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.dziewulskij.passwordswallet.domain.authority.AuthorityUtil;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Getter
@Builder
@EqualsAndHashCode
public class UserPrincipal implements UserDetails {

	private final Long id;
	private final String email;
	private final String username;
	private final boolean enable;
	private final boolean locked;

	@JsonIgnore
	private final String password;
	private final Collection<? extends GrantedAuthority> authorities;

	public static UserPrincipal build(User user) {
		var authorities = AuthorityUtil.getAuthorityNames(user);
		return UserPrincipal.builder()
				.id(user.getId())
				.email(user.getEmail())
				.username(user.getUsername())
				.enable(user.isEnable())
				.locked(user.isLocked())
				.password(user.getPassword())
				.authorities(authorities)
				.build();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enable;
	}

}

