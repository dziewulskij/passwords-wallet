package pl.dziewulskij.passwordswallet.core.secure;

import java.util.Base64;
import java.util.Random;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SaltGenerator {

	private static final Random secureRandom = new Random();

	public static String generate() {
		byte[] salt = new byte[20];
		secureRandom.nextBytes(salt);
		return Base64.getEncoder()
				.encodeToString(salt)
				.substring(0, 20);
	}
}
