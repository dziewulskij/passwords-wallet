package pl.dziewulskij.passwordswallet.core.secure.hash;

import pl.dziewulskij.passwordswallet.core.common.Algorithm;

public interface Hash {

	boolean isSupport(Algorithm algorithm);

	String compute(String text);

}
