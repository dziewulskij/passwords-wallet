package pl.dziewulskij.passwordswallet.core.secure.hash;

import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Component;

import pl.dziewulskij.passwordswallet.core.common.Algorithm;

@Component
public class HashStrategy {

	private final Set<Hash> hashes;

	public HashStrategy(final Set<Hash> hashes) {
		this.hashes = hashes;
	}

	public String compute(Algorithm algorithm, String password, String salt) {
		var passwordWithSalt = concatSaltWithPassword(salt, password);
		return hashes.stream()
				.filter(hash -> hash.isSupport(algorithm))
				.findFirst()
				.map(hash -> hash.compute(passwordWithSalt))
				.orElseThrow(IllegalStateException::new);
	}

	private String concatSaltWithPassword(String salt, String password) {
		return Optional.ofNullable(salt)
				.map(value -> value.concat(password))
				.orElse(password);
	}

}
