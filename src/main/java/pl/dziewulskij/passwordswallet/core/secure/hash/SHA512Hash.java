package pl.dziewulskij.passwordswallet.core.secure.hash;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;

@Slf4j
@Component
public class SHA512Hash implements Hash {

	@Value("${security.pepper}")
	private String pepper;

	@Override
	public boolean isSupport(Algorithm algorithm) {
		return algorithm.equals(Algorithm.SHA512);
	}

	@Override
	public String compute(String text) {
		return Try.of(() -> generateSHA512(text))
				.onFailure(this::logErrorAndThrowRuntimeException)
				.get();
	}

	private String generateSHA512(String text) throws NoSuchAlgorithmException {
		var messageDigest = MessageDigest.getInstance(Algorithm.SHA512.getName());
		messageDigest.update(pepper.concat(text).getBytes(StandardCharsets.UTF_8));
		return String.format("%064x", new BigInteger(1, messageDigest.digest()));
	}

	private void logErrorAndThrowRuntimeException(Throwable thr) {
		log.error("generateSHA512 ERROR: {}", thr.getMessage());
		throw new RuntimeException(thr);
	}
}
