package pl.dziewulskij.passwordswallet.core.secure.hash;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;

@Slf4j
@Component
public class MD5Hash implements Hash {

	@Override
	public boolean isSupport(Algorithm algorithm) {
		return algorithm.equals(Algorithm.MD5);
	}

	@Override
	public String compute(String text) {
		return Try.of(() -> generateMD5(text))
				.onFailure(this::logErrorAndThrowRuntimeException)
				.get();
	}

	private String generateMD5(String text) throws NoSuchAlgorithmException {
		var messageDigest = MessageDigest.getInstance(Algorithm.MD5.getName());
		byte[] digest = messageDigest.digest(text.getBytes(StandardCharsets.UTF_8));
		return String.format("%032x", new BigInteger(1, digest));
	}

	private void logErrorAndThrowRuntimeException(Throwable thr) {
		log.error("generateMD5 ERROR: {}", thr.getMessage());
		throw new RuntimeException(thr);
	}
}
