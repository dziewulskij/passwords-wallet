package pl.dziewulskij.passwordswallet.core.secure;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import io.vavr.control.Try;
import pl.dziewulskij.passwordswallet.core.exception.security.BadDecryptOperationException;
import pl.dziewulskij.passwordswallet.core.exception.security.BadEncryptOperationException;

public class AESenc {

	private static final String ALGORITHM = "AES";

	public static String encrypt(String decryptedData, Key key) {
		return Try.of(() -> tryEncrypt(decryptedData, key))
				.map(val -> Base64.getEncoder().encodeToString(val))
				.getOrElseThrow(BadEncryptOperationException::new);
	}

	public static String decrypt(String encryptedData, Key key) {
		return Try.of(() -> tryDecrypt(encryptedData, key))
				.map(String::new)
				.getOrElseThrow(BadDecryptOperationException::new);
	}

	public static Key generateKey(String key) {
		return new SecretKeySpec(key.getBytes(), ALGORITHM);
	}

	private static byte[] tryEncrypt(String decryptedData, Key key) throws Exception {
		var cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(decryptedData.getBytes());
	}

	public static byte[] tryDecrypt(String encryptedData, Key key) throws Exception {
		var cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decodeValue = Base64.getDecoder().decode(encryptedData);
		return cipher.doFinal(decodeValue);
	}

}
