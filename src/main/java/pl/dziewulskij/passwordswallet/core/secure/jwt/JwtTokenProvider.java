package pl.dziewulskij.passwordswallet.core.secure.jwt;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import static pl.dziewulskij.passwordswallet.core.common.CustomClaim.AUTHORITIES;
import static pl.dziewulskij.passwordswallet.core.common.CustomClaim.USER_ID;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.authority.AuthorityUtil;

@Slf4j
@Component
public class JwtTokenProvider {

	@Value("${jwt.key.secret}")
	private String jwtSecret;

	@Value("${jwt.expiration-in-ms}")
	private int jwtExpirationInMs;

	@Value("${spring.application.name}")
	private String jwtSubject;

	public String generateToken(Authentication authentication) {
		var userPrincipal = (UserPrincipal) authentication.getPrincipal();
		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

		return Jwts.builder()
				.setSubject(jwtSubject)
				.setIssuedAt(new Date())
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.claim(USER_ID.getName(), userPrincipal.getId())
				.claim(AUTHORITIES.getName(), AuthorityUtil.getAuthorityNames(userPrincipal))
				.compact();
	}

	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (SignatureException ex) {
			log.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	}

}
