package pl.dziewulskij.passwordswallet.core.secure.hash;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;

@Slf4j
@Component
public class HMACHash implements Hash {

	@Value("${security.hmac.key}")
	private String hmacKey;

	@Override
	public boolean isSupport(Algorithm algorithm) {
		return algorithm.equals(Algorithm.HMAC);
	}

	@Override
	public String compute(String text) {
		final byte[] keyBytes = hmacKey.getBytes(StandardCharsets.UTF_8);
		return Try.of(() -> generateHMAC(text, keyBytes))
				.onFailure(this::logErrorAndThrowRuntimeException)
				.get();
	}

	private String generateHMAC(String text, byte[] keyBytes) throws NoSuchAlgorithmException, InvalidKeyException {
		var sha512HMAC = Mac.getInstance(Algorithm.HMAC.getName());
		var secretKeySpec = new SecretKeySpec(keyBytes, Algorithm.HMAC.getName());
		sha512HMAC.init(secretKeySpec);
		byte[] macData = sha512HMAC.doFinal(text.getBytes(StandardCharsets.UTF_8));
		return Base64.getEncoder().encodeToString(macData);
	}

	private void logErrorAndThrowRuntimeException(Throwable thr) {
		log.error("generateHMAC ERROR: {}", thr.getMessage());
		throw new RuntimeException(thr);
	}
}
