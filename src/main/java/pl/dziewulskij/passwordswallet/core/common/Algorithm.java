package pl.dziewulskij.passwordswallet.core.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Algorithm {

	MD5("MD5"),
	SHA512("SHA-512"),
	HMAC("HmacSHA512");

	private final String name;
}
