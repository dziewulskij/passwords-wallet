package pl.dziewulskij.passwordswallet.core.common;

public enum ActionType {

	CREATE,
	UPDATE,
	DELETE
}
