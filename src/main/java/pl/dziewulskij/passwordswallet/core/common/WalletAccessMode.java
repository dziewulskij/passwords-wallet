package pl.dziewulskij.passwordswallet.core.common;

public enum WalletAccessMode {

	MODIFY,
	VIEW
}
