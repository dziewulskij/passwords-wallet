package pl.dziewulskij.passwordswallet.core.common;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AuthorityName implements GrantedAuthority {

	ROLE_USER(AuthorityCode.ROLE_USER),
	ROLE_ADMIN(AuthorityCode.ROLE_ADMIN);

	private final String name;

	@Override
	public String getAuthority() {
		return name;
	}

	public static class AuthorityCode {
		public static final String ROLE_USER = "ROLE_USER";
		public static final String ROLE_ADMIN = "ROLE_ADMIN";
	}
}
