package pl.dziewulskij.passwordswallet.core.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CustomClaim {

	USER_ID("userId"),
	AUTHORITIES("authorities");

	private final String name;

}
