package pl.dziewulskij.passwordswallet.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	private static final long MAX_AGE_SECS = 3600;

	@Value("${cors.allowed-methods}")
	private String[] allowedMethods;

	@Value("${cors.allowed-origins}")
	private String allowedOrigins;

	@Value("${cors.mapping}")
	private String mapping;

	@Override
	public void addCorsMappings(CorsRegistry corsRegistry) {
		corsRegistry.addMapping(mapping)
				.allowedOrigins(allowedOrigins)
				.allowedMethods(allowedMethods)
				.maxAge(MAX_AGE_SECS);
	}
}
