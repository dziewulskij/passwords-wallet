package pl.dziewulskij.passwordswallet.config;

import java.util.Optional;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.user.model.User;

@Slf4j
@Configuration
public class JpaAuditorAwareConfig implements AuditorAware<User> {

	@Override
	public Optional<User> getCurrentAuditor() {
		return Try.of(this::buildUser)
				.toJavaOptional();
	}

	private User buildUser() {
		var jwtUser = JwtUtil.getJwtUser();
		return User.builder()
				.id(jwtUser.getId())
				.build();
	}

}
