package pl.dziewulskij.passwordswallet.webui.rest.user.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class UserDto extends SimpleUserDto {

	private Long id;
	private boolean enable;
	private boolean locked;
	private WalletAccessMode walletAccessMode;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;

}
