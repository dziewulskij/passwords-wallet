package pl.dziewulskij.passwordswallet.webui.rest.auth;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.auth.AuthenticationService;
import pl.dziewulskij.passwordswallet.webui.rest.auth.dto.JwtDto;
import pl.dziewulskij.passwordswallet.webui.rest.auth.dto.UserLoginDto;

@Slf4j
@RestController
@RequestMapping("/api/authentication")
public class AuthenticationResource {

	private final AuthenticationService authenticationService;

	public AuthenticationResource(final AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	@PostMapping
	public ResponseEntity<JwtDto> login(@Valid @RequestBody UserLoginDto userLoginDto,
										HttpServletRequest httpServletRequest) {
		log.info("login: {}", userLoginDto);
		var jwtDto = new JwtDto(authenticationService.login(userLoginDto, httpServletRequest));
		return ResponseEntity.ok(jwtDto);
	}

}
