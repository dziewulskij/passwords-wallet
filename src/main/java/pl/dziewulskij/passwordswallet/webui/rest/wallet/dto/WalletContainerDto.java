package pl.dziewulskij.passwordswallet.webui.rest.wallet.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Builder(toBuilder = true)
public class WalletContainerDto {

	private List<WalletDto> ownWallets;
	private List<WalletDto> sharedWallets;

}
