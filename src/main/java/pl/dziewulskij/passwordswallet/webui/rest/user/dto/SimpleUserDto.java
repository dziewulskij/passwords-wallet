package pl.dziewulskij.passwordswallet.webui.rest.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.EMAIL_PATTERN;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class SimpleUserDto {

	@NotBlank
	@Size(max = 50)
	private String name;

	@NotBlank
	@Size(max = 50)
	private String surname;

	@NotBlank
	@Size(max = 100)
	@Pattern(regexp = EMAIL_PATTERN)
	private String email;

	@NotBlank
	@Size(min = 3, max = 30)
	private String username;

}
