package pl.dziewulskij.passwordswallet.webui.rest.user.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.dziewulskij.passwordswallet.core.common.LoginStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@Builder(toBuilder = true)
public class UserLoginEventDto {

	private Long id;
	private LoginStatus loginStatus;
	private String ipAddress;
	private String operationSystem;
	private String browserVersion;
	private LocalDateTime loginTime;

}
