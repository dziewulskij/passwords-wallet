package pl.dziewulskij.passwordswallet.webui.rest.user.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UserValidatorConstant {

	public static final String USERNAME_UNIQUE_ERROR = "user.username.not.unique.error";
	public static final String EMAIL_UNIQUE_ERROR = "user.email.not.unique.error";
	public static final String CONFIRMATION_PASSWORD_EQUAL_ERROR = "user.confirmation.password.not.match.error";

	public static final String USERNAME_FIELD = "username";
	public static final String EMAIL_FIELD = "email";
	public static final String CONFIRMATION_PASSWORD_FIELD = "confirmationPassword";

	public static final String EMAIL_PATTERN = "^[a-zA-z0-9]+[\\._a-zA-Z0-9]*@[a-zA-Z0-9]{2,}\\.[a-zA-Z]{2,}[\\.a-zA-Z0-9]*$";
	public static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\+\\-\\=])(?=.*[A-Z])(?!.*\\s).{8,}$";

}
