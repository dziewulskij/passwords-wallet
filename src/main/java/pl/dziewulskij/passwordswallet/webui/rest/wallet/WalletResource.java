package pl.dziewulskij.passwordswallet.webui.rest.wallet;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.wallet.service.WalletService;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.SimpleWalletDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletContainerDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletDto;
import pl.dziewulskij.passwordswallet.webui.rest.wallet.validator.WalletValidator;

@Slf4j
@RestController
@RequestMapping("/api/wallets")
public class WalletResource {

	private final WalletService walletService;
	private final WalletValidator walletValidator;

	public WalletResource(final WalletService walletService,
						  final WalletValidator walletValidator) {
		this.walletService = walletService;
		this.walletValidator = walletValidator;
	}

	@PostMapping
	public ResponseEntity<WalletDto> createWallet(@Valid @RequestBody SimpleWalletDto simpleWalletDto) {
		log.info("createWallet: {}", simpleWalletDto);
		return ResponseEntity.ok(walletService.createWallet(simpleWalletDto));
	}

	@GetMapping("/{id}")
	public ResponseEntity<WalletDto> getWalletById(@PathVariable Long id) {
		log.info("getWalletById: {}", id);
		walletValidator.validateIfWalletBelongsOrIsSharedForUser(id);
		return ResponseEntity.ok(walletService.getWalletById(id));
	}

	@GetMapping
	public ResponseEntity<WalletContainerDto> getAllUserWallets() {
		log.info("getAllUserWallets");
		return ResponseEntity.ok(walletService.getAllUserWallets());
	}

	@PutMapping("/{id}")
	public ResponseEntity<WalletDto> editWalletById(@PathVariable Long id,
													@Valid @RequestBody SimpleWalletDto simpleWalletDto) {
		log.info("editWalletById: {}, object: {}", id, simpleWalletDto);
		walletValidator.checkIfWalletBelongsToUserAndIfUserHasModifyMode(id);
		return ResponseEntity.ok(walletService.editWalletById(id, simpleWalletDto));
	}

	@PutMapping("/share/{walletId}")
	public ResponseEntity<Void> shareWalletByIdFor(@PathVariable Long walletId, @RequestParam String email) {
		log.info("shareWalletByIdFor: {}, for: {}", walletId, email);
		walletValidator.validateShareWalletParams(walletId, email);
		walletService.shareWalletByIdFor(walletId, email);
		return ResponseEntity.ok().build();
	}

	@PutMapping("/reject-shared/{walletId}/users/{userId}")
	public ResponseEntity<Void> rejectSharedWalletByIdFor(@PathVariable Long walletId, @PathVariable Long userId) {
		log.info("rejectShareWalletByIdFor: {}, for: {}", walletId, userId);
		walletValidator.validateRejectSharedWalletParams(walletId, userId);
		walletService.rejectSharedWalletByIdFor(walletId, userId);
		return ResponseEntity.ok().build();
	}

	@PutMapping("/rollback/aud/{walletAudId}")
	public ResponseEntity<WalletDto> rollbackWalletByWalletAudId(@PathVariable Long walletAudId) {
		log.info("rollbackWalletByWalletAudId: {}", walletAudId);
		walletValidator.checkIfWalletBelongsToUserByWalletAudIdAndIfUserHasModifyMode(walletAudId);
		return ResponseEntity.ok(walletService.rollbackWalletByWalletAudId(walletAudId));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteWalletById(@PathVariable Long id) {
		log.info("deleteWalletById: {}", id);
		walletValidator.checkIfWalletBelongsToUserAndIfUserHasModifyMode(id);
		walletService.deleteWalletById(id);
		return ResponseEntity.ok().build();
	}

}
