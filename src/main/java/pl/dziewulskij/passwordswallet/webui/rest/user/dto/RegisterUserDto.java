package pl.dziewulskij.passwordswallet.webui.rest.user.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.PASSWORD_PATTERN;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.core.common.Algorithm;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
@ToString(exclude = {"password", "confirmationPassword"})
public class RegisterUserDto extends SimpleUserDto {

	@NotBlank
	@Size(min = 8, max = 20)
	@Pattern(regexp = PASSWORD_PATTERN)
	private String password;

	@NotBlank
	@Size(min = 8, max = 20)
	private String confirmationPassword;

	private Algorithm algorithm;

}
