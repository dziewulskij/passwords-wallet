package pl.dziewulskij.passwordswallet.webui.rest.wallet.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.dziewulskij.passwordswallet.domain.dictionary.DictionaryDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class WalletDto extends SimpleWalletDto {

	private Long id;
	private DictionaryDto owner;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private DictionaryDto createdBy;
	private DictionaryDto lastModifiedBy;

}
