package pl.dziewulskij.passwordswallet.webui.rest.auth.dto;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString(exclude = {"password"})
public class UserLoginDto {

	@NotBlank
	private final String username;

	@NotBlank
	private final String password;

}
