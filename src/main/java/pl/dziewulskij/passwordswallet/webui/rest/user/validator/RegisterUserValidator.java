package pl.dziewulskij.passwordswallet.webui.rest.user.validator;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.CONFIRMATION_PASSWORD_EQUAL_ERROR;
import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.CONFIRMATION_PASSWORD_FIELD;
import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.EMAIL_FIELD;
import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.EMAIL_UNIQUE_ERROR;
import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.USERNAME_FIELD;
import static pl.dziewulskij.passwordswallet.webui.rest.user.validator.UserValidatorConstant.USERNAME_UNIQUE_ERROR;

import pl.dziewulskij.passwordswallet.domain.user.repository.UserRepository;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto;

@Component
public class RegisterUserValidator implements Validator {

	private final UserRepository userRepository;

	public RegisterUserValidator(final UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterUserDto.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		RegisterUserDto registerUserDto = (RegisterUserDto) object;
		checkUniqueUsername(registerUserDto.getUsername(), errors);
		checkUniqueEmail(registerUserDto.getEmail(), errors);
		checkConfirmationPasswords(registerUserDto.getPassword(), registerUserDto.getConfirmationPassword(), errors);
	}

	private void checkUniqueUsername(String username, Errors errors) {
		Optional.of(username)
				.filter(userRepository::existsByUsername)
				.ifPresent(val -> errors.rejectValue(USERNAME_FIELD, USERNAME_UNIQUE_ERROR));
	}

	private void checkUniqueEmail(String email, Errors errors) {
		Optional.of(email)
				.filter(userRepository::existsByEmail)
				.ifPresent(val -> errors.rejectValue(EMAIL_FIELD, EMAIL_UNIQUE_ERROR));
	}

	private void checkConfirmationPasswords(String password, String confirmationPassword, Errors errors) {
		if (!password.equals(confirmationPassword)) {
			errors.rejectValue(CONFIRMATION_PASSWORD_FIELD, CONFIRMATION_PASSWORD_EQUAL_ERROR);
		}
	}
}
