package pl.dziewulskij.passwordswallet.webui.rest.ipaddress;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static pl.dziewulskij.passwordswallet.core.common.AuthorityName.AuthorityCode.ROLE_ADMIN;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.ipaddress.service.IpAddressService;

@Slf4j
@RestController
@RequestMapping("/api/ip-addresses")
public class IpAddressResource {

	private final IpAddressService ipAddressService;

	public IpAddressResource(final IpAddressService ipAddressService) {
		this.ipAddressService = ipAddressService;
	}

	@Secured(ROLE_ADMIN)
	@PutMapping("/unlock/{ip-address}")
	public ResponseEntity<Void> unlock(@PathVariable("ip-address") String ipAddress) {
		log.info("unlock ip address {}", ipAddress);
		ipAddressService.unlockByIpAddress(ipAddress);
		return ResponseEntity.ok().build();
	}

}
