package pl.dziewulskij.passwordswallet.webui.rest.wallet.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Builder))
@SuperBuilder(toBuilder = true)
public class SimpleWalletDto {

	@NotBlank
	@Size(max = 30)
	private String login;

	@NotBlank
	@Size(max = 256)
	private String password;

	@NotBlank
	@Size(max = 256)
	private String webAddress;

	@Size(max = 256)
	private String description;

}
