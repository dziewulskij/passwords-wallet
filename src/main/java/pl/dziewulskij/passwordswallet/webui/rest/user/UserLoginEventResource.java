package pl.dziewulskij.passwordswallet.webui.rest.user;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.domain.user.service.UserLoginEventService;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserLoginEventDto;

@Slf4j
@RestController
@RequestMapping("/api/user-login-events")
public class UserLoginEventResource {

	private final UserLoginEventService userLoginEventService;

	public UserLoginEventResource(final UserLoginEventService userLoginEventService) {
		this.userLoginEventService = userLoginEventService;
	}

	@GetMapping
	public ResponseEntity<List<UserLoginEventDto>> getUserLoginEvents() {
		log.info("getUserLoginEvents");
		return ResponseEntity.ok(userLoginEventService.getUserLoginEvents());
	}

}
