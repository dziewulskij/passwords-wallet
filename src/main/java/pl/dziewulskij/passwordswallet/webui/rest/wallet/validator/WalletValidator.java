package pl.dziewulskij.passwordswallet.webui.rest.wallet.validator;

import java.util.Optional;

import org.springframework.stereotype.Component;

import io.vavr.control.Try;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException;
import pl.dziewulskij.passwordswallet.core.exception.ValidationException;
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUtil;
import pl.dziewulskij.passwordswallet.domain.user.model.User;
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet;
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletAudRepository;
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletRepository;

@Component
public class WalletValidator {

	private final WalletRepository walletRepository;
	private final WalletAudRepository walletAudRepository;

	public WalletValidator(final WalletRepository walletRepository,
						   final WalletAudRepository walletAudRepository) {
		this.walletRepository = walletRepository;
		this.walletAudRepository = walletAudRepository;
	}

	public void checkIfWalletBelongsToUserByWalletAudIdAndIfUserHasModifyMode(Long walletAudId) {
		walletAudRepository.findWalletAudWalletId(walletAudId)
				.ifPresentOrElse(
						this::checkIfWalletBelongsToUserAndIfUserHasModifyMode,
						ObjectNotFoundException::thrown
				);
	}

	public void checkIfWalletBelongsToUserAndIfUserHasModifyMode(Long walletId) {
		var jwtUser = JwtUtil.getJwtUser();
		checkIfWalletBelongsToUser(walletId, jwtUser.getId());
		checkIfUserHasModifyMode(jwtUser.getWalletAccessMode());
	}

	public void validateShareWalletParams(Long walletId, String email) {
		Optional.of(email)
				.filter(mail -> !mail.equals(JwtUtil.getJwtUser().getEmail()))
				.orElseThrow(() -> new ValidationException("You cannot share the object with yourself"));

		checkIfWalletBelongsToUserAndIfUserHasModifyMode(walletId);
	}

	public void validateRejectSharedWalletParams(Long walletId, Long userId) {
		var jwtUser = JwtUtil.getJwtUser();
		Try.run(() -> checkIfWalletBelongsToUser(walletId, jwtUser.getId()))
				.onFailure(thr -> checkIfUserIdBelongAndWalletIsSharedForUser(walletId, userId, jwtUser.getId()));
	}

	public void validateIfWalletBelongsOrIsSharedForUser(Long walletId) {
		var jwtUser = JwtUtil.getJwtUser();
		Try.run(() -> checkIfWalletBelongsToUser(walletId, jwtUser.getId()))
				.onFailure(thr -> checkIfWalletIsSharedForUser(walletId, jwtUser.getId()));
	}

	private void checkIfWalletBelongsToUser(Long walletId, Long userId) {
		walletRepository.findOwnerIdByWalletId(walletId)
				.filter(ownerId -> ownerId.equals(userId))
				.orElseThrow(() -> new ValidationException("You must be the owner of the wallet"));
	}

	private void checkIfUserIdBelongAndWalletIsSharedForUser(Long walletId, Long userId, Long loggedUserId) {
		checkIfWalletIsSharedForUser(walletId, loggedUserId);
		Optional.of(userId)
				.filter(id -> id.equals(loggedUserId))
				.orElseThrow(() -> new ValidationException("The wallet should be shared to you"));
	}

	private void checkIfWalletIsSharedForUser(Long walletId, Long loggedUserId) {
		walletRepository.findById(walletId)
				.map(Wallet::getSharedForUsers)
				.map(users -> users.stream()
						.map(User::getId)
						.filter(id -> id.equals(loggedUserId))
						.findAny()
						.orElseThrow(() -> new ValidationException("You must be the owner of the wallet or the password must be shared with you"))
				);
	}

	private void checkIfUserHasModifyMode(WalletAccessMode walletAccessMode) {
		if (walletAccessMode == WalletAccessMode.VIEW) {
			throw new ValidationException("You must be in MODIFY mode");
		}
	}

}
