package pl.dziewulskij.passwordswallet.webui.rest.user;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode;
import pl.dziewulskij.passwordswallet.domain.user.service.UserService;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto;
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserDto;
import pl.dziewulskij.passwordswallet.webui.rest.user.validator.RegisterUserValidator;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserResource {

	private final RegisterUserValidator registerUserValidator;
	private final UserService userService;

	public UserResource(final RegisterUserValidator registerUserValidator,
						final UserService userService) {
		this.registerUserValidator = registerUserValidator;
		this.userService = userService;
	}

	@InitBinder(value = "registerUserDto")
	public void initUserCreateValidator(WebDataBinder webDataBinder) {
		webDataBinder.setValidator(registerUserValidator);
	}

	@PostMapping
	public ResponseEntity<UserDto> registerUser(@Valid @RequestBody RegisterUserDto registerUserDto) {
		log.info("createUser: {}", registerUserDto);
		return ResponseEntity.ok(userService.registerUser(registerUserDto));
	}

	@PutMapping("/mode")
	public ResponseEntity<Void> changeWalletAccessMode(@RequestParam("wallet-access-mode") WalletAccessMode walletAccessMode) {
		log.info("changeWalletAccessMode: {}", walletAccessMode);
		userService.changeWalletAccessMode(walletAccessMode);
		return ResponseEntity.ok().build();
	}
}
