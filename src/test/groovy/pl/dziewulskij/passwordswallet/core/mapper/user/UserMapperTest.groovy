package pl.dziewulskij.passwordswallet.core.mapper.user

import org.mapstruct.factory.Mappers
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.util.user.RegisterUserDtoFactory
import pl.dziewulskij.passwordswallet.util.user.UserFactory
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserDto
import spock.lang.Specification

class UserMapperTest extends Specification {

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class)

    def 'mapToUserDto should map User to UserDto'() {
        given:
        def user = UserFactory.buildUser()

        when:
        UserDto result = userMapper.mapToUserDto(user)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == user.id
            result.name == user.name
            result.surname == user.surname
            result.email == user.email
            result.username == user.username
            result.enable == user.enable
            result.locked == user.locked
            result.walletAccessMode == user.walletAccessMode
            result.createdAt == user.createdAt
            result.updatedAt == user.updatedAt
        }
    }

    def 'mapToUser should map RegisterUserDto to User'() {
        given:
        def registerUserDto = RegisterUserDtoFactory.buildRegisterUserDto()

        when:
        User result = userMapper.mapToUser(registerUserDto)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.name == registerUserDto.name
            result.surname == registerUserDto.surname
            result.email == registerUserDto.email
            result.username == registerUserDto.username
            result.password == registerUserDto.password
            result.algorithm == registerUserDto.algorithm
            result.walletAccessMode == WalletAccessMode.VIEW
        }
    }

}
