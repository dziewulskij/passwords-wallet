package pl.dziewulskij.passwordswallet.core.mapper.dictionary

import org.mapstruct.factory.Mappers
import pl.dziewulskij.passwordswallet.domain.dictionary.DictionaryDto
import pl.dziewulskij.passwordswallet.util.user.UserFactory
import spock.lang.Specification

class DictionaryMapperTest extends Specification {

    private DictionaryMapper dictionaryMapper = Mappers.getMapper(DictionaryMapper.class)

    def 'mapToDictionaryDto should map User to DictionaryDto'() {
        given:
        def user = UserFactory.buildUser()

        when:
        DictionaryDto result = dictionaryMapper.mapToDictionaryDto(user)

        then:
        Objects.nonNull(result)
        result.id == user.id
        result.name == user.name + ' ' + user.surname
    }

}
