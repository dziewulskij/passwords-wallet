package pl.dziewulskij.passwordswallet.core.mapper.user

import org.mapstruct.factory.Mappers
import pl.dziewulskij.passwordswallet.util.user.UserLoginEventFactory
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserLoginEventDto
import spock.lang.Specification

class UserLoginEventMapperTest extends Specification {

    private UserLoginEventMapper userLoginEventMapper = Mappers.getMapper(UserLoginEventMapper.class)

    def 'should map UserLoginEvent to UserLoginEventDto'() {
        given:
        def userLoginEvent = UserLoginEventFactory.buildUserLoginEvent()

        when:
        UserLoginEventDto result = userLoginEventMapper.mapToUserLoginEventDto(userLoginEvent)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == userLoginEvent.id
            result.loginStatus == userLoginEvent.loginStatus
            result.ipAddress == userLoginEvent.ipAddress
            result.operationSystem == userLoginEvent.operationSystem
            result.browserVersion == userLoginEvent.browserVersion
            result.loginTime == userLoginEvent.loginTime
        }
    }

}
