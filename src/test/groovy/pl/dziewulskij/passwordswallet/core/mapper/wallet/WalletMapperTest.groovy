package pl.dziewulskij.passwordswallet.core.mapper.wallet

import org.mapstruct.factory.Mappers
import pl.dziewulskij.passwordswallet.core.mapper.dictionary.DictionaryMapper
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud
import pl.dziewulskij.passwordswallet.util.wallet.SimpleWalletDtoFactory
import pl.dziewulskij.passwordswallet.util.wallet.WalletAudFactory
import pl.dziewulskij.passwordswallet.util.wallet.WalletFactory
import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.WalletDto
import spock.lang.Specification

class WalletMapperTest extends Specification {

    private DictionaryMapper dictionaryMapper = Mappers.getMapper(DictionaryMapper.class)
    private WalletMapper walletMapper = new WalletMapperImpl(dictionaryMapper)

    def 'mapToWallet should map SimpleWalletDto to Wallet'() {
        given:
        def simpleWalletDto = SimpleWalletDtoFactory.buildSimpleWalletDto()

        when:
        Wallet result = walletMapper.mapToWallet(simpleWalletDto)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.login == simpleWalletDto.login
            result.password == simpleWalletDto.password
            result.webAddress == simpleWalletDto.webAddress
            result.description == simpleWalletDto.description
        }
    }

    def 'mapToWallet should map SimpleWalletDto with target Wallet to Wallet'() {
        given:
        def simpleWalletDto = SimpleWalletDtoFactory.buildSimpleWalletDto()
        def wallet = WalletFactory.buildWallet()

        when:
        Wallet result = walletMapper.mapToWallet(simpleWalletDto, wallet)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == wallet.id
            result.login == simpleWalletDto.login
            result.password == simpleWalletDto.password
            result.webAddress == simpleWalletDto.webAddress
            result.description == simpleWalletDto.description
            wallet.removed == wallet.removed
            result.user == wallet.user
            result.sharedForUsers == wallet.sharedForUsers
        }
    }

    def 'mapToWalletDto should map Wallet to WalletDto'() {
        given:
        def wallet = WalletFactory.buildWallet()

        when:
        WalletDto result = walletMapper.mapToWalletDto(wallet)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == wallet.id
            result.login == wallet.login
            result.password == wallet.password
            result.webAddress == wallet.webAddress
            result.description == wallet.description
            wallet.removed == wallet.removed
            result.owner.id == wallet.user.id
            result.owner.name == wallet.user.name + ' ' + wallet.user.surname
            result.createdAt == wallet.createdAt
            result.updatedAt == wallet.updatedAt
            result.createdBy.id == wallet.createdBy.id
            result.lastModifiedBy.id == wallet.lastModifiedBy.id
        }
    }

    def 'mapToWalletAud should map Wallet to WalletAud'() {
        given:
        def wallet = WalletFactory.buildWallet()

        when:
        WalletAud result = walletMapper.mapToWalletAud(wallet)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == null
            result.login == wallet.login
            result.password == wallet.password
            result.webAddress == wallet.webAddress
            result.description == wallet.description
            wallet.removed == wallet.removed
            result.user == wallet.user
        }
    }

    def 'mapToWallet should map WalletAud with target Wallet to Wallet'() {
        given:
        def wallet = WalletFactory.buildWallet()
        def walletAud = WalletAudFactory.buildWalletAudFactory()

        when:
        Wallet result = walletMapper.mapToWallet(walletAud, wallet)

        then:
        verifyAll {
            Objects.nonNull(result)
            result.id == wallet.id
            result.login == walletAud.login
            result.password == walletAud.password
            result.webAddress == walletAud.webAddress
            result.description == walletAud.description
            wallet.removed == walletAud.removed
            result.user == walletAud.user
            result.sharedForUsers == wallet.sharedForUsers
            result.createdAt == wallet.createdAt
            result.updatedAt == wallet.updatedAt
        }
    }

}
