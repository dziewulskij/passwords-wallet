package pl.dziewulskij.passwordswallet

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtUser
import spock.lang.Specification

abstract class AbstractSecurityContext extends Specification {

    protected prepareSecurityContextHolder() {
        def securityContext = Mock(SecurityContext)
        def authentication = Mock(Authentication)
        def jwtUser = JwtUser.builder().id(1L).password('password').build()
        authentication.principal >> jwtUser
        securityContext.authentication >> authentication
        SecurityContextHolder.setContext(securityContext)
    }

}
