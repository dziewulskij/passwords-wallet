package pl.dziewulskij.passwordswallet.domain.auth

import org.springframework.mock.web.MockHttpServletRequest
import pl.dziewulskij.passwordswallet.core.common.LoginStatus
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode
import pl.dziewulskij.passwordswallet.domain.ipaddress.IpAddressEventManager
import pl.dziewulskij.passwordswallet.domain.user.UserLoginEventManager
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class AuthenticationManagerFacadeTest extends Specification {

    private UserLoginEventManager userLoginEventManager = Mock()
    private IpAddressEventManager ipAddressEventManager = Mock()
    private UserDaoService userDaoService = Mock()
    private AuthenticationManagerFacade authenticationManagerFacade = new AuthenticationManagerFacade(
            userLoginEventManager,
            ipAddressEventManager,
            userDaoService
    )

    def 'checkIfIpAddressOrAccountIsNonLocked should invoke methods'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'

        when:
        authenticationManagerFacade.checkIfIpAddressOrAccountIsNonLocked(httpServletRequest, user)

        then:
        1 * ipAddressEventManager.checkIfIpAddressIsNonLocked(_ as String)
        1 * userLoginEventManager.checkIfAccountIsNonLocked(_ as User)
    }

    def 'onSuccessAuthenticate should invoke methods when loginEvent not thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'
        user.id >> 1L

        when:
        authenticationManagerFacade.onSuccessAuthenticate(httpServletRequest, user)

        then:
        1 * userLoginEventManager.loginEvent(_ as HttpServletRequest, _ as User, _ as LoginStatus)
        1 * ipAddressEventManager.onSuccessAuthenticate(_ as HttpServletRequest)
        1 * userDaoService.updateWalletAccessMode(_ as Long, _ as WalletAccessMode)
    }

    def 'onSuccessAuthenticate should invoke methods when loginEvent thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'
        user.id >> 1L
        1 * userLoginEventManager.loginEvent(_ as HttpServletRequest, _ as User, _ as LoginStatus) >> { throw new RuntimeException() }

        when:
        authenticationManagerFacade.onSuccessAuthenticate(httpServletRequest, user)

        then:
        1 * ipAddressEventManager.onSuccessAuthenticate(_ as HttpServletRequest)
        0 * userDaoService.updateWalletAccessMode(_ as Long, _ as WalletAccessMode)
        thrown(RuntimeException)
    }

    def 'onFailureAuthenticate should invoke methods when loginEvent not thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'

        when:
        authenticationManagerFacade.onFailureAuthenticate(httpServletRequest, user)

        then:
        1 * userLoginEventManager.loginEvent(_ as HttpServletRequest, _ as User, _ as LoginStatus)
        1 * ipAddressEventManager.onFailureAuthenticate(_ as HttpServletRequest)
    }

    def 'onFailureAuthenticate should invoke methods when loginEvent thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'
        1 * userLoginEventManager.loginEvent(_ as HttpServletRequest, _ as User, _ as LoginStatus) >> { throw new RuntimeException() }

        when:
        authenticationManagerFacade.onFailureAuthenticate(httpServletRequest, user)

        then:
        1 * ipAddressEventManager.onFailureAuthenticate(_ as HttpServletRequest)
        thrown(RuntimeException)
    }

    def 'onFinallyAuthenticate should invoke methods when onFinallyAuthenticate not thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'
        1 * userLoginEventManager.onFinallyAuthenticate(_ as User)

        when:
        authenticationManagerFacade.onFinallyAuthenticate(httpServletRequest, user)

        then:
        1 * ipAddressEventManager.onFinallyAuthenticate(_ as HttpServletRequest)
    }

    def 'onFinallyAuthenticate should invoke methods when onFinallyAuthenticate thrown error'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        httpServletRequest.remoteAddr >> '10.10.20.30'
        user.username >> 'dziewulskij'
        1 * userLoginEventManager.onFinallyAuthenticate(_ as User) >> { throw new RuntimeException() }

        when:
        authenticationManagerFacade.onFinallyAuthenticate(httpServletRequest, user)

        then:
        1 * ipAddressEventManager.onFinallyAuthenticate(_ as HttpServletRequest)
        thrown(RuntimeException)
    }


}
