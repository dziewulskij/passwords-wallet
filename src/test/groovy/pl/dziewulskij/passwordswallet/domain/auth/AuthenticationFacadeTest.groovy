package pl.dziewulskij.passwordswallet.domain.auth

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import pl.dziewulskij.passwordswallet.core.common.Algorithm
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy
import pl.dziewulskij.passwordswallet.core.secure.jwt.JwtTokenProvider
import spock.lang.Specification

import static pl.dziewulskij.passwordswallet.core.common.Algorithm.SHA512

class AuthenticationFacadeTest extends Specification {

    private AuthenticationManager authenticationManager = Mock()
    private JwtTokenProvider jwtTokenProvider = Mock()
    private HashStrategy hashStrategy = Mock()
    private AuthenticationFacade authenticationFacade = new AuthenticationFacade(
            authenticationManager,
            jwtTokenProvider,
            hashStrategy
    )

    def 'compute should invoke compute method from HashStrategy'() {
        given:
        1 * hashStrategy.compute(_ as Algorithm, _ as String, _ as String) >> 'test'

        when:
        def result = authenticationFacade.compute(SHA512, 'password', 'salt')

        then:
        Objects.nonNull(result)
        result == 'test'
    }

    def 'authenticate should invoke authenticate method from Authentication'() {
        given:
        def usernamePasswordAuthenticationToken = Mock(UsernamePasswordAuthenticationToken)
        1 * authenticationManager.authenticate(_ as UsernamePasswordAuthenticationToken) >> Mock(Authentication)

        when:
        def result = authenticationFacade.authenticate(usernamePasswordAuthenticationToken)

        then:
        Objects.nonNull(result)
        result instanceof Authentication
    }

    def 'generateToken should invoke generateToken method from Authentication'() {
        given:
        1 * jwtTokenProvider.generateToken(_ as Authentication) >> 'token'

        when:
        def result = authenticationFacade.generateToken(Mock(Authentication))

        then:
        Objects.nonNull(result)
        result == 'token'
    }

}
