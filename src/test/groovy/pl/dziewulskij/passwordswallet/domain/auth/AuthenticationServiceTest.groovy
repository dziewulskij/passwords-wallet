package pl.dziewulskij.passwordswallet.domain.auth

import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import pl.dziewulskij.passwordswallet.core.common.Algorithm
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService
import pl.dziewulskij.passwordswallet.util.user.UserLoginDtoFactory
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class AuthenticationServiceTest extends Specification {

    private AuthenticationFacade authenticateFacade = Mock()
    private UserDaoService userDaoService = Mock()
    private AuthenticationManagerFacade authenticationManagerFacade = Mock()
    private AuthenticationService authenticationService = new AuthenticationService(
            authenticateFacade,
            userDaoService,
            authenticationManagerFacade
    )

    def 'login should correctly authenticate user'() {
        given:
        def userLoginDto = UserLoginDtoFactory.buildUserLoginDto()
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        user.algorithm >> Algorithm.SHA512
        user.salt >> 'salt'
        SecurityContextHolder.setContext(Mock(SecurityContext))

        1 * userDaoService.findUserByUsername(_ as String) >> Optional.of(user)
        1 * authenticationManagerFacade.checkIfIpAddressOrAccountIsNonLocked(_ as HttpServletRequest, _ as User)
        1 * authenticateFacade.compute(_ as Algorithm, _ as String, _ as String) >> 'password'
        1 * authenticateFacade.authenticate(_ as UsernamePasswordAuthenticationToken) >> Mock(Authentication)
        1 * authenticateFacade.generateToken(_ as Authentication) >> 'token'
        1 * authenticationManagerFacade.onSuccessAuthenticate(_ as HttpServletRequest, _ as User)
        0 * authenticationManagerFacade.onFailureAuthenticate(_ as HttpServletRequest, _ as User)
        1 * authenticationManagerFacade.onFinallyAuthenticate(_ as HttpServletRequest, _ as User)

        when:
        def result = authenticationService.login(userLoginDto, httpServletRequest)

        then:
        Objects.nonNull(result)
        result == 'token'
    }

    def 'login should incorrectly authenticate user'() {
        given:
        def userLoginDto = UserLoginDtoFactory.buildUserLoginDto()
        def httpServletRequest = new MockHttpServletRequest()
        def user = Mock(User)
        user.algorithm >> Algorithm.SHA512
        user.salt >> 'salt'
        SecurityContextHolder.setContext(Mock(SecurityContext))

        1 * userDaoService.findUserByUsername(_ as String) >> Optional.of(user)
        1 * authenticationManagerFacade.checkIfIpAddressOrAccountIsNonLocked(_ as HttpServletRequest, _ as User)
        1 * authenticateFacade.compute(_ as Algorithm, _ as String, _ as String) >> 'password'
        1 * authenticateFacade.authenticate(_ as UsernamePasswordAuthenticationToken)
        0 * authenticationManagerFacade.onSuccessAuthenticate(_ as HttpServletRequest, _ as User)
        1 * authenticationManagerFacade.onFailureAuthenticate(_ as HttpServletRequest, _ as User)
        1 * authenticationManagerFacade.onFinallyAuthenticate(_ as HttpServletRequest, _ as User)

        when:
        authenticationService.login(userLoginDto, httpServletRequest)

        then:
        def exc = thrown(BadCredentialsException)
        exc.message == 'Bad credentials'
    }

    def 'login should incorrectly authenticate when user by username not found'() {
        given:
        def userLoginDto = UserLoginDtoFactory.buildUserLoginDto()
        def httpServletRequest = new MockHttpServletRequest()

        1 * userDaoService.findUserByUsername(_ as String) >> Optional.empty()

        when:
        authenticationService.login(userLoginDto, httpServletRequest)

        then:
        def exc = thrown(BadCredentialsException)
        exc.message == 'Bad username'
    }

}
