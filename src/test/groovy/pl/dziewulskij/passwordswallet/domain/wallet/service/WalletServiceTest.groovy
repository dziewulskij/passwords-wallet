package pl.dziewulskij.passwordswallet.domain.wallet.service

import pl.dziewulskij.passwordswallet.AbstractSecurityContext
import pl.dziewulskij.passwordswallet.core.mapper.wallet.WalletMapper
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy

class WalletServiceTest extends AbstractSecurityContext {

    private WalletDaoService walletDaoService = Mock()
    private WalletAudService walletAudService = Mock()
    private WalletMapper walletMapper = Mock()
    private HashStrategy hashStrategy = Mock()
    private WalletService walletService = new WalletService(
            walletDaoService,
            walletAudService,
            walletMapper,
            hashStrategy
    )


}
