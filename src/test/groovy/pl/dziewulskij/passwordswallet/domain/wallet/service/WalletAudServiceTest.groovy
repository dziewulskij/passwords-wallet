package pl.dziewulskij.passwordswallet.domain.wallet.service

import pl.dziewulskij.passwordswallet.core.common.ActionType
import pl.dziewulskij.passwordswallet.core.mapper.wallet.WalletMapper
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet
import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletAudRepository
import spock.lang.Specification

class WalletAudServiceTest extends Specification {

    private WalletAudRepository walletAudRepository = Mock()
    private WalletMapper walletMapper = Mock()
    private WalletAudService walletAudService = new WalletAudService(walletAudRepository, walletMapper)

    def 'create should save WalletAud'() {
        given:
        def wallet = Mock(Wallet)
        wallet.sharedForUsers >> Set.of(Mock(User))
        1 * walletMapper.mapToWalletAud(_ as Wallet) >> WalletAud.builder().build()

        when:
        walletAudService.create(wallet, ActionType.CREATE)

        then:
        1 * walletAudRepository.save(_ as WalletAud)
    }

    def 'findWalletAudById should retrieve Optional of WalletAud'() {
        given:
        1 * walletAudRepository.findById(_ as Long) >> Optional.of(Mock(WalletAud))

        when:
        def result = walletAudService.findWalletAudById(1L)

        then:
        Objects.nonNull(result)
        result instanceof Optional<WalletAud>
    }


}
