package pl.dziewulskij.passwordswallet.domain.wallet.service

import pl.dziewulskij.passwordswallet.core.common.ActionType
import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.user.repository.UserRepository
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet
import pl.dziewulskij.passwordswallet.domain.wallet.repository.WalletRepository
import pl.dziewulskij.passwordswallet.AbstractSecurityContext

class WalletDaoServiceTest extends AbstractSecurityContext {

    private UserRepository userRepository = Mock()
    private WalletRepository walletRepository = Mock()
    private WalletAudService walletAudService = Mock()
    private WalletDaoService walletDaoService = new WalletDaoService(
            userRepository,
            walletRepository,
            walletAudService
    )

    def 'createWallet should create new Wallet'() {
        given:
        prepareSecurityContextHolder()
        def wallet = Mock(Wallet)
        1 * userRepository.findById(_ as Long) >> Optional.of(Mock(User))
        1 * walletRepository.save(_ as Wallet) >> Mock(Wallet)
        1 * walletAudService.create(_ as Wallet, _ as ActionType)

        when:
        def result = walletDaoService.createWallet(wallet)

        then:
        Objects.nonNull(result)
        result instanceof Wallet
    }

    def 'createWallet should thrown ObjectNotFoundException'() {
        given:
        prepareSecurityContextHolder()
        def wallet = Mock(Wallet)
        1 * userRepository.findById(_ as Long) >> Optional.empty()

        when:
        walletDaoService.createWallet(wallet)

        then:
        thrown(ObjectNotFoundException)
    }

    def 'findWalletById should retrieve Optional of Wallet'() {
        given:
        1 * walletRepository.findById(_ as Long) >> Optional.of(Mock(Wallet))

        when:
        def result = walletDaoService.findWalletById(1L)

        then:
        Objects.nonNull(result)
        result instanceof Optional<Wallet>
    }

    def 'findAllUserWallets should retrieve list of Wallet'() {
        given:
        1 * walletRepository.findAllWalletsByUserId(_ as Long) >> List.of(Mock(Wallet))

        when:
        def result = walletDaoService.findAllUserWallets(1L)

        then:
        Objects.nonNull(result)
        result.size() == 1
        result[0] instanceof Wallet
    }

    def 'findAllUserSharedWallets should retrieve list of Wallet'() {
        given:
        1 * walletRepository.findAllSharedWalletsFor(_ as Long) >> List.of(Mock(Wallet))

        when:
        def result = walletDaoService.findAllUserSharedWallets(1L)

        then:
        Objects.nonNull(result)
        result.size() == 1
        result[0] instanceof Wallet
    }

    def 'save should save and retrieve Wallet'() {
        given:
        1 * walletRepository.save(_ as Wallet) >> Mock(Wallet)
        1 * walletAudService.create(_ as Wallet, _ as ActionType)

        when:
        def result = walletDaoService.save(Mock(Wallet), ActionType.CREATE)

        then:
        Objects.nonNull(result)
        result instanceof Wallet
    }

    def 'delete should delete wallet'() {
        given:
        1 * walletRepository.save(_ as Wallet) >> Mock(Wallet)

        when:
        walletDaoService.delete(Mock(Wallet))

        then:
        1 * walletAudService.create(_ as Wallet, _ as ActionType)
    }

    def 'sharePasswordFor should share wallet for user'() {
        given:
        1 * userRepository.findByEmail(_ as String) >> Optional.of(Mock(User))
        1 * walletRepository.save(_ as Wallet) >> Mock(Wallet)
        1 * walletAudService.create(_ as Wallet, _ as ActionType)

        when:
        def result = walletDaoService.shareWalletFor(Mock(Wallet), 'dziewulskij')

        then:
        Objects.nonNull(result)
        result instanceof Wallet
    }

    def 'shareWalletFor should thrown ObjectNotFoundException'() {
        given:
        1 * userRepository.findByEmail(_ as String) >> Optional.empty()

        when:
        walletDaoService.shareWalletFor(Mock(Wallet), 'dziewulskij')

        then:
        thrown(ObjectNotFoundException)
    }

    def 'rejectSharedWalletFor should reject shared wallet'() {
        given:
        def wallet = Mock(Wallet)
        wallet.sharedForUsers >> Set.of(User.builder().id(1L).build())
        1 * walletRepository.save(_ as Wallet) >> Mock(Wallet)
        1 * walletAudService.create(_ as Wallet, _ as ActionType)

        when:
        def result = walletDaoService.rejectSharedWalletFor(wallet, 1L)

        then:
        Objects.nonNull(result)
        result instanceof Wallet
    }

}
