package pl.dziewulskij.passwordswallet.domain.user.service

import pl.dziewulskij.passwordswallet.AbstractSecurityContext
import pl.dziewulskij.passwordswallet.core.mapper.user.UserLoginEventMapper
import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserLoginEventDaoService
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserLoginEventDto

class UserLoginEventServiceTest extends AbstractSecurityContext {

    private UserLoginEventDaoService userLoginEventDaoService = Mock()
    private UserLoginEventMapper userLoginEventMapper = Mock()
    private UserLoginEventService userLoginEventService = new UserLoginEventService(
            userLoginEventDaoService,
            userLoginEventMapper
    )

    def 'getUserLoginEvents should retrieve List of UserLoginEventDto'() {
        given:
        prepareSecurityContextHolder()
        1 * userLoginEventDaoService.findAllUserLoginEventByUserId(_ as Long) >> List.of(Mock(UserLoginEvent))
        1 * userLoginEventMapper.mapToUserLoginEventDto(_ as UserLoginEvent) >> Mock(UserLoginEventDto)

        when:
        def result = userLoginEventService.getUserLoginEvents()

        then:
        Objects.nonNull(result)
        result.size() == 1
        result[0] instanceof UserLoginEventDto
    }

}
