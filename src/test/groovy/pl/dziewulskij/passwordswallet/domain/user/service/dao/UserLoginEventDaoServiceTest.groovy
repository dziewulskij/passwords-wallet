package pl.dziewulskij.passwordswallet.domain.user.service.dao

import pl.dziewulskij.passwordswallet.core.common.LoginStatus
import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent
import pl.dziewulskij.passwordswallet.domain.user.repository.UserLoginEventRepository
import spock.lang.Specification

class UserLoginEventDaoServiceTest extends Specification {

    private UserLoginEventRepository userLoginEventRepository = Mock()
    private UserLoginEventDaoService userLoginEventDaoService = new UserLoginEventDaoService(userLoginEventRepository)

    def 'save should save UserLoginEvent'() {
        when:
        userLoginEventDaoService.save(Mock(UserLoginEvent))

        then:
        1 * userLoginEventRepository.save(_ as UserLoginEvent)
    }

    def 'find4LastLoginStatuses should retrieve list of last 4 login statuses'() {
        given:
        def userLoginEvent = Mock(UserLoginEvent)
        userLoginEvent.loginStatus >> LoginStatus.SUCCESS
        1 * userLoginEventRepository.findTop4ByUserIdOrderByLoginTimeDesc(_ as Long) >> List.of(userLoginEvent)

        when:
        def result = userLoginEventDaoService.find4LastLoginStatuses(1L)

        then:
        Objects.nonNull(result)
        result.size() == 1
        result[0] instanceof LoginStatus
    }

    def 'findAllUserLoginEventByUserId should retrieve list of UserLoginEvent'() {
        given:
        1 * userLoginEventRepository.findAllByUserId(_ as Long) >> List.of(Mock(UserLoginEvent))

        when:
        def result = userLoginEventDaoService.findAllUserLoginEventByUserId(1L)

        then:
        Objects.nonNull(result)
        result.size() == 1
        result[0] instanceof UserLoginEvent
    }
}
