package pl.dziewulskij.passwordswallet.domain.user.service

import pl.dziewulskij.passwordswallet.AbstractSecurityContext
import pl.dziewulskij.passwordswallet.core.common.Algorithm
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode
import pl.dziewulskij.passwordswallet.core.mapper.user.UserMapper
import pl.dziewulskij.passwordswallet.core.secure.hash.HashStrategy
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.user.service.dao.UserDaoService
import pl.dziewulskij.passwordswallet.util.user.UserFactory
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.UserDto

class UserServiceTest extends AbstractSecurityContext {

    private UserMapper userMapper = Mock()
    private UserDaoService userDaoService = Mock()
    private HashStrategy hashStrategy = Mock()
    private UserService userService = new UserService(userMapper, userDaoService, hashStrategy)

    def 'registerUser should create user'() {
        given:
        def registerUserDto = Mock(RegisterUserDto)
        def user = UserFactory.buildUser()
        1 * userMapper.mapToUser(_ as RegisterUserDto) >> user
        1 * hashStrategy.compute(_ as Algorithm, _ as String, _ as String)
        1 * userDaoService.createUser(_ as User) >> Mock(User)
        1 * userMapper.mapToUserDto(_ as User) >> Mock(UserDto)

        when:
        def result = userService.registerUser(registerUserDto)

        then:
        Objects.nonNull(result)
        result instanceof UserDto
    }

    def 'changeWalletAccessMode should change access mode'() {
        given:
        prepareSecurityContextHolder()

        when:
        userService.changeWalletAccessMode(WalletAccessMode.MODIFY)

        then:
        1 * userDaoService.updateWalletAccessMode(_ as Long, _ as WalletAccessMode)
    }


}
