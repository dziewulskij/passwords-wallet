package pl.dziewulskij.passwordswallet.domain.user.service.dao

import pl.dziewulskij.passwordswallet.core.common.AuthorityName
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode
import pl.dziewulskij.passwordswallet.core.secure.jwt.UserPrincipal
import pl.dziewulskij.passwordswallet.domain.authority.AuthorityRepository
import pl.dziewulskij.passwordswallet.domain.authority.model.Authority
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.user.repository.UserRepository
import pl.dziewulskij.passwordswallet.util.user.UserFactory
import spock.lang.Specification

class UserDaoServiceTest extends Specification {

    private UserRepository userRepository = Mock()
    private AuthorityRepository authorityRepository = Mock()
    private UserDaoService userDaoService = new UserDaoService(userRepository, authorityRepository)

    def 'findUserPrincipalByUsername should retrieve Optional of UserPrincipal'() {
        given:
        def user = UserFactory.buildUser()
        1 * userRepository.findByUsername(_ as String) >> Optional.of(user)

        when:
        def result = userDaoService.findUserPrincipalByUsername('dziewulskij')

        then:
        Objects.nonNull(result)
        result instanceof Optional<UserPrincipal>
    }

    def 'findUserById should retrieve Optional of User'() {
        given:
        1 * userRepository.findById(_ as Long) >> Optional.of(Mock(User))

        when:
        def result = userDaoService.findUserById(1L)

        then:
        Objects.nonNull(result)
        result instanceof Optional<User>
    }

    def 'findUserByUsername should retrieve Optional of User'() {
        given:
        1 * userRepository.findByUsername(_ as String) >> Optional.of(Mock(User))

        when:
        def result = userDaoService.findUserByUsername('dziewulskij')

        then:
        Objects.nonNull(result)
        result instanceof Optional<User>
    }

    def 'createUser should create and retrieve User'() {
        given:
        def user = UserFactory.buildUser()
        1 * authorityRepository.findByAuthorityName(_ as AuthorityName) >> Optional.of(Authority.builder().build())
        1 * userRepository.save(_ as User) >> Mock(User)

        when:
        def result = userDaoService.createUser(user)

        then:
        Objects.nonNull(result)
        result instanceof User
    }

    def 'save should save and retrieve saved User'() {
        given:
        1 * userRepository.save(_ as User) >> Mock(User)

        when:
        def result = userDaoService.save(Mock(User))

        then:
        Objects.nonNull(result)
        result instanceof User
    }

    def 'updateWalletAccessMode should update access mode'() {
        when:
        userDaoService.updateWalletAccessMode(1L, WalletAccessMode.MODIFY)

        then:
        1 * userRepository.updateWalletAccessMode(_ as Long, _ as WalletAccessMode)
    }

}
