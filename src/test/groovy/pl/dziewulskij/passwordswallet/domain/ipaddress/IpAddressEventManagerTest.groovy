package pl.dziewulskij.passwordswallet.domain.ipaddress

import org.springframework.mock.web.MockHttpServletRequest
import pl.dziewulskij.passwordswallet.core.exception.security.IpAddressLockedException
import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress
import pl.dziewulskij.passwordswallet.domain.ipaddress.service.IpAddressDaoService
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime

class IpAddressEventManagerTest extends Specification {

    private IpAddressDaoService ipAddressDaoService = Mock()
    private IpAddressEventManager ipAddressEventManager = new IpAddressEventManager(ipAddressDaoService)

    def 'checkIfIpAddressIsNonLocked should not thrown exception'() {
        given:
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(Mock(IpAddress))

        when:
        ipAddressEventManager.checkIfIpAddressIsNonLocked('10.20.30.40')

        then:
        notThrown()
    }

    def 'checkIfIpAddressIsNonLocked should thrown exception'() {
        given:
        def ipAddress = IpAddress.builder().unlockedAt(LocalDateTime.now().plusDays(1)).build()
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(ipAddress)

        when:
        ipAddressEventManager.checkIfIpAddressIsNonLocked('10.20.30.40')

        then:
        thrown(IpAddressLockedException)
    }

    def 'onSuccessAuthenticate should update IpAddress'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        httpServletRequest.remoteAddr >> '10.20.30.40'
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(Mock(IpAddress))

        when:
        ipAddressEventManager.onSuccessAuthenticate(httpServletRequest)

        then:
        1 * ipAddressDaoService.save(_ as IpAddress)
    }

    def 'onFailureAuthenticate should update IpAddress'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        httpServletRequest.remoteAddr >> '10.20.30.40'
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(Mock(IpAddress))

        when:
        ipAddressEventManager.onFailureAuthenticate(httpServletRequest)

        then:
        1 * ipAddressDaoService.save(_ as IpAddress)
    }

    def 'onFailureAuthenticate should create IpAddress'() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        httpServletRequest.remoteAddr >> '10.20.30.40'
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.empty()

        when:
        ipAddressEventManager.onFailureAuthenticate(httpServletRequest)

        then:
        1 * ipAddressDaoService.save(_ as IpAddress)
    }

    @Unroll('where incorrectAttempts: #incorrectAttempts')
    def 'onFinallyAuthenticate should update IpAddress and thrown IpAddressLockedException '() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def ipAddress = Mock(IpAddress)
        httpServletRequest.remoteAddr >> '10.20.30.40'
        ipAddress.incorrectAttempts >> incorrectAttempts
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(ipAddress)
        invokSetUnlockAt * ipAddress.setUnlockedAt(_ as LocalDateTime)
        invokeSetPermanentlyLocked * ipAddress.setPermanentlyLocked(_ as Boolean)

        when:
        ipAddressEventManager.onFinallyAuthenticate(httpServletRequest)

        then:
        1 * ipAddressDaoService.save(_ as IpAddress)
        thrown(IpAddressLockedException)

        where:
        incorrectAttempts || invokSetUnlockAt | invokeSetPermanentlyLocked
        2                 || 1                | 0
        3                 || 1                | 0
        4                 || 0                | 1
    }

    def 'onFinallyAuthenticate should not update IpAddress and not thrown IpAddressLockedException '() {
        given:
        def httpServletRequest = new MockHttpServletRequest()
        def ipAddress = Mock(IpAddress)
        httpServletRequest.remoteAddr >> '10.20.30.40'
        ipAddress.incorrectAttempts >> 1
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(ipAddress)

        when:
        ipAddressEventManager.onFinallyAuthenticate(httpServletRequest)

        then:
        0 * ipAddressDaoService.save(_ as IpAddress)
        notThrown(IpAddressLockedException)
    }

}
