package pl.dziewulskij.passwordswallet.domain.ipaddress.service

import pl.dziewulskij.passwordswallet.core.exception.ObjectNotFoundException
import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress
import spock.lang.Specification

class IpAddressServiceTest extends Specification {

    private IpAddressDaoService ipAddressDaoService = Mock()
    private IpAddressService ipAddressService = new IpAddressService(ipAddressDaoService)

    def 'unlockByIpAddress should unlock ip address'() {
        when:
        ipAddressService.unlockByIpAddress('10.20.30.40')

        then:
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.of(Mock(IpAddress))
        1 * ipAddressDaoService.save(_ as IpAddress) >> Mock(IpAddress)
    }

    def 'unlockByIpAddress should thrown ObjectNotFoundException'() {
        given:
        1 * ipAddressDaoService.findByIpAddress(_ as String) >> Optional.empty()

        when:
        ipAddressService.unlockByIpAddress('10.20.30.40')

        then:
        thrown(ObjectNotFoundException)
    }

}
