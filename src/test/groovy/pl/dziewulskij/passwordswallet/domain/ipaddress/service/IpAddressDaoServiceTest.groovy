package pl.dziewulskij.passwordswallet.domain.ipaddress.service

import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress
import pl.dziewulskij.passwordswallet.domain.ipaddress.repository.IpAddressRepository
import spock.lang.Specification

class IpAddressDaoServiceTest extends Specification {

    private IpAddressRepository ipAddressRepository = Mock()
    private IpAddressDaoService ipAddressDaoService = new IpAddressDaoService(ipAddressRepository)

    def 'findByIpAddress should retrieve Optional of IpAddress'() {
        given:
        1 * ipAddressRepository.findByIpAddress(_ as String) >> Optional.of(Mock(IpAddress))

        when:
        def result = ipAddressDaoService.findByIpAddress('10.15.20.30')

        then:
        Objects.nonNull(result)
        result instanceof Optional<IpAddress>
    }

    def 'findByIpAddress should save and retrieve IpAddress'() {
        given:
        1 * ipAddressRepository.save(_ as IpAddress) >> Mock(IpAddress)

        when:
        def result = ipAddressDaoService.save(Mock(IpAddress))

        then:
        Objects.nonNull(result)
        result instanceof IpAddress
    }

}
