package pl.dziewulskij.passwordswallet.domain.ipaddress

import pl.dziewulskij.passwordswallet.domain.ipaddress.model.IpAddress
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime

class CheckLockPredicateTest extends Specification {

    private CheckLockPredicate checkLockPredicate = new CheckLockPredicate()

    @Unroll('for permanentlyLocked: #permanentlyLocked, unlockedAt: #unlockedAt then expectResult: #expectResult')
    def 'check predicate test method for IpAddress '() {
        given:
        def ipAddress = IpAddress.builder()
                .permanentlyLocked(permanentlyLocked)
                .unlockedAt(unlockedAt)
                .build()

        when:
        def result = checkLockPredicate.test(ipAddress)

        then:
        result == expectResult

        where:
        unlockedAt                       | permanentlyLocked || expectResult
        null                             | false             || false
        LocalDateTime.now().minusDays(1) | false             || false
        LocalDateTime.now().minusDays(1) | true              || true
        LocalDateTime.now().plusDays(1)  | false             || true
        LocalDateTime.now().plusDays(1)  | true              || true
    }

}
