package pl.dziewulskij.passwordswallet.util.wallet

import org.assertj.core.util.Sets
import pl.dziewulskij.passwordswallet.domain.user.model.User
import pl.dziewulskij.passwordswallet.domain.wallet.domain.Wallet
import pl.dziewulskij.passwordswallet.util.user.UserFactory

import java.time.LocalDateTime

class WalletFactory {

    static Wallet buildWallet() {
        Wallet.builder()
                .id(1L)
                .login('WalletLogin')
                .password('Wallet')
                .webAddress('Wallet')
                .description('Wallet')
                .removed(false)
                .user(UserFactory.buildUser())
                .sharedForUsers(Sets.newHashSet())
                .createdAt(LocalDateTime.now().minusDays(2))
                .updatedAt(LocalDateTime.now().minusDays(2))
                .createdBy(User.builder().id(2L).build())
                .lastModifiedBy(User.builder().id(2L).build())
                .build()
    }

}
