package pl.dziewulskij.passwordswallet.util.wallet

import pl.dziewulskij.passwordswallet.domain.wallet.domain.audit.WalletAud
import pl.dziewulskij.passwordswallet.util.user.UserFactory

import java.time.LocalDateTime

class WalletAudFactory {

    static WalletAud buildWalletAudFactory() {
        WalletAud.builder()
                .id(1L)
                .login('WalletLogin')
                .password('Wallet')
                .webAddress('Wallet')
                .description('Wallet')
                .removed(false)
                .user(UserFactory.buildUser())
                .createdAt(LocalDateTime.now().minusDays(2))
                .updatedAt(LocalDateTime.now().minusDays(2))
                .build()
    }

}
