package pl.dziewulskij.passwordswallet.util.wallet

import pl.dziewulskij.passwordswallet.webui.rest.wallet.dto.SimpleWalletDto

class SimpleWalletDtoFactory {

    static SimpleWalletDto buildSimpleWalletDto() {
        SimpleWalletDto.builder()
                .login('SimpleWalletDtoLogin')
                .password('SimpleWalletDtoPassword')
                .webAddress('SimpleWalletDtoWebAddress')
                .description('SimpleWalletDtoDesc')
                .build()
    }

}
