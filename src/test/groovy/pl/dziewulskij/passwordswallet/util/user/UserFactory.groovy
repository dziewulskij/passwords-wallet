package pl.dziewulskij.passwordswallet.util.user

import org.assertj.core.util.Sets
import pl.dziewulskij.passwordswallet.core.common.Algorithm
import pl.dziewulskij.passwordswallet.core.common.AuthorityName
import pl.dziewulskij.passwordswallet.core.common.WalletAccessMode
import pl.dziewulskij.passwordswallet.domain.authority.model.Authority
import pl.dziewulskij.passwordswallet.domain.user.model.User

import java.time.LocalDateTime

class UserFactory {

    static User buildUser() {
        User.builder()
                .id(1L)
                .email('email@email.pl')
                .username('dziewulskij')
                .enable(true)
                .locked(false)
                .algorithm(Algorithm.SHA512)
                .password('password')
                .authorities(Sets.newHashSet(Set.of(buildAuthority())))
                .walletAccessMode(WalletAccessMode.VIEW)
                .createdAt(LocalDateTime.now().minusDays(1))
                .updatedAt(LocalDateTime.now().minusDays(1))
                .build()
    }

    private static Authority buildAuthority() {
        Authority.builder()
                .authorityName(AuthorityName.ROLE_USER)
                .build()
    }

}
