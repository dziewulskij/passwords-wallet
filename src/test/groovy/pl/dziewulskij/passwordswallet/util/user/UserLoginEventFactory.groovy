package pl.dziewulskij.passwordswallet.util.user

import pl.dziewulskij.passwordswallet.core.common.LoginStatus
import pl.dziewulskij.passwordswallet.domain.user.model.UserLoginEvent

import java.time.LocalDateTime

class UserLoginEventFactory {

    static UserLoginEvent buildUserLoginEvent() {
        UserLoginEvent.builder()
                .id(1L)
                .loginStatus(LoginStatus.SUCCESS)
                .userId(2L)
                .ipAddress('10.20.30.40')
                .operationSystem('Win 10')
                .browserVersion('2.0.3.1')
                .loginTime(LocalDateTime.now().minusDays(2))
                .build()
    }

}
