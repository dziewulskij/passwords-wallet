package pl.dziewulskij.passwordswallet.util.user

import pl.dziewulskij.passwordswallet.webui.rest.auth.dto.UserLoginDto

class UserLoginDtoFactory {

    static UserLoginDto buildUserLoginDto() {
        new UserLoginDto('dziewulskij', 'password')
    }

}
