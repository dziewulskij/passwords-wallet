package pl.dziewulskij.passwordswallet.util.user

import pl.dziewulskij.passwordswallet.core.common.Algorithm
import pl.dziewulskij.passwordswallet.webui.rest.user.dto.RegisterUserDto

class RegisterUserDtoFactory {

    static buildRegisterUserDto() {
        RegisterUserDto.builder()
                .name('RegisterUserDtoName')
                .surname('RegisterUserDtoSurname')
                .email('RegisterUserDtoEmail')
                .username('RegisterUserDtoUsername')
                .password('RegisterUserDtoPassword')
                .confirmationPassword('RegisterUserDtoPassword')
                .algorithm(Algorithm.SHA512)
                .build()
    }

}
